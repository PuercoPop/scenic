
(defpackage :scenic-utils
  (:use :cl)
  (:export max-box  draw-button-raw pass-to-child
           ifhorizontal let-from-options
           fill-list groups
           gen-print-object gen-serializer
           yes-no-query set2val1
           validate-layout-spec is-auto sorted-auto-indices
           intersperse color
           define-vm-accessor))

(defpackage :scenic
  (:use :cl :scenic-utils :mabu :iterate)
  (:export run-scene
           eventful add-event-handler remove-event-handler on-event
           widget widget-class measured-width measured-height visible
           name auto-name
           set-measured
           layout-left layout-top layout-width layout-height
           set-layout
           parent
           measure layout paint paint-scene
           container stack children
           proportional-container
           padding padding-left padding-top padding-right padding-bottom child
           border stroke-color stroke-width
           background fill-color
           placeholder width height
           event handled
           mouse-event mouse-x mouse-y mouse-button modifiers mouse-move-event
           mouse-rel-x mouse-rel-y mouse-button-event
           widget-list
           key-event key unicode
           scene invalidate-scene
           filler
           textattr
           label text font-face font-size font-color font-slant font-weight
           button
           clickable
           stateful state
           toggle-button
           slider min-value max-value page-size current-min-position
           sizer min-width min-height max-width max-height
           adjuster expander translator scaler rotator
           capture-mouse release-mouse
           invalidate
           arrow direction
           scrollbar
           orientable orientation
           image image-path get-image
           svg-image fa-icon
           aligner
           clipper circular-clipper
           glass opacity
           henchman
           placer
           adjuster
           scroll-view horizontal-offset vertical-offset
           inside-width inside-height
           scroll-view-measured-event inner-width inner-height outer-width outer-height
           textbox cursor-position selection-start caret-color selection-color
           calculate-focusables
           *test-channel-enabled* *event-recording-enabled* *session-record*
           reset-event-log test-channel-write
           serialize
           replay-scene-session
           resource read-stream-to-string
           read-resource
           write-gzipped-resource
           read-gzipped-resource
           checkbox
           radio-button
           pack space-between-cells
           add-ui-task *replay-task-timeout-ms*
           allocate-thread on-ui-thread check-ui-thread with-thread
           on-scene-init as-ui-task as-delayed-ui-task
           as-async-task publish-progress update-progress start-async-task-processors
           defstylesheet defstyle-rule
           find-widget find-widgets find-widget-by-name
           component component-class defcomponent))

(defpackage :scenic-grid
  (:use :cl :scenic :scenic-utils :mabu)
  (:export grid))

(defpackage :scenic-helpers
  (:use :cl :scenic :scenic-grid :scenic-utils :mabu)
  (:export scene background border
           placeholder
           padding uniform-padding
           vertical-pack horizontal-pack
           filler
           stack
           clickable
           label button button-text toggle
           horizontal-slider vertical-slider
           sizer arrow
           horizontal-scrollbar vertical-scrollbar
           image grid
           aligner
           clipper O-clipper
           glass
           henchman
           translator
           scroll-view scroll-view-auto
           textbox
           checkbox
           radio-button
           group-stateful-buttons
           simple-horizontal-pack simple-vertical-pack
           pack
           with-bindings
           ->*))

(defpackage :scenic-test
  (:use :cl :scenic-helpers :scenic-utils)
  (:export run-all-tests run-auto-tests))

(defpackage :scenic.draw
  (:use :cl)
  (:export
   :set-color
   :parse-color-or-pattern
   :parse-unit
   :draw-svg
   :alpha-color
   :set-fill-extent
   :linear-gradient
   :apply-linear-gradient
   :brighten
   :with-drawing-context

   ;; cairo

   destroy deg-to-rad version
   rgba red green blue alpha hsv->rgb

   ;; surface
   *surface* 
   surface pointer width height pixel-based-p destroy
   create-ps-surface create-pdf-surface create-svg-surface
   ps-surface-set-size pdf-surface-set-size
   create-recording-surface
   create-image-surface create-similar-image
   create-image-surface-for-data create-image-surface-for-array
   image-surface-get-format image-surface-get-width
   image-surface-get-height image-surface-get-data
   image-surface-get-stride image-surface-create-from-png
   image-surface-create-from-png-callback
   image-surface-create-from-png-stream
   with-context-from-surface with-surface-and-context
   surface-write-to-png with-surface with-png-surface
   create-surface-from-foreign
   surface-flush surface-finish surface-mark-dirty

   ;; context

   context *context* with-context with-png-file create-context sync
   sync-lock sync sync-unlock sync-reset with-sync-lock save restore
   push-group pop-group pop-group-to-source set-source-rgb
   set-source-rgba clip clip-preserve reset-clip copy-page show-page
   fill-preserve paint paint-with-alpha stroke stroke-preserve
   set-source-color get-line-width set-line-width get-miter-limit
   set-miter-limit get-antialias set-antialias get-fill-rule
   set-fill-rule get-line-cap set-line-cap get-line-join set-line-join
   get-operator set-operator fill-path set-dash get-dash clip-extents
   fill-extents in-fill in-stoke create-ps-context create-pdf-context
   create-svg-context get-target mask-surface set-source-surface

   ;; pattern

   pattern create-rgb-pattern create-rgba-pattern
   create-linear-pattern create-radial-pattern
   create-pattern-for-surface pattern-add-color-stop-rgb
   pattern-add-color-stop-rgba pattern-add-color-stop pattern-get-type
   pattern-set-matrix pattern-get-matrix pattern-set-extend
   pattern-get-extend pattern-set-filet pattern-get-filter set-source
   mask create-color-pattern with-linear-pattern with-radial-pattern
   with-patterns

   create-mesh-pattern pattern-mesh-begin-patch pattern-mesh-end-patch
   pattern-mesh-move-to pattern-mesh-line-to pattern-mesh-curve-to
   pattern-mesh-set-control-point
   pattern-mesh-set-corner-color-rgb pattern-mesh-set-corner-color-rgba
   pattern-mesh-get-patch-count pattern-mesh-get-control-point
   pattern-mesh-get-corner-rgba

   pattern-get-rgba pattern-get-surface get-source
   pattern-get-color-stop-rgba pattern-get-color-stop-count
   pattern-get-color-stops pattern-get-linear-points
   pattern-get-radial-circles

   ;; path

   new-path new-sub-path close-path arc arc-negative curve-to line-to
   move-to rectangle rel-move-to rel-curve-to rel-line-to text-path
   has-current-point get-current-point

   ;; text

   select-font-face set-font-size text-extents show-text
   text-x-bearing text-y-bearing
   text-width text-height
   text-x-advance text-y-advance
   get-text-extents
   font-ascent font-descent font-height
   font-max-x-advance font-max-y-advance
   get-font-extents

   glyph-array glyph-array-count glyph-array-filled
   make-glyph-array glyph-array-add glyph-array-set-glyph
   glyph-array-reset-fill

   set-font-matrix get-font-matrix set-font-options get-font-options
   set-font-face get-font-face set-scaled-font get-scaled-font
   show-glyphs glyph-extents

   ;; font

   font-face scaled-font font-options

   create-font set-font

   create-scaled-font scaled-font-extents scaled-font-text-extents
   scaled-font-glyph-extents scaled-font-get-type scaled-font-get-ctm
   scaled-font-get-font-matrix scaled-font-get-scale-matrix
   scaled-font-face

   create-font-options font-options-copy font-options-merge font-options-hash
   font-options-equal font-options-set-antialias font-options-get-antialias
   font-options-set-subpixel-order font-options-get-subpixel-order
   font-options-set-hint-style font-options-get-hint-style
   font-options-set-hint-metrics font-options-get-hint-metrics

   ;; user-font

   user-font-face

   ;; transformations

   translate scale rotate reset-trans-matrix make-trans-matrix
   trans-matrix-xx trans-matrix-yx trans-matrix-xy trans-matrix-yy
   trans-matrix-x0 trans-matrix-y0 trans-matrix-p transform
   set-trans-matrix get-trans-matrix user-to-device
   user-to-device-distance device-to-user device-to-user-distance
   trans-matrix-init-translate trans-matrix-init-scale
   trans-matrix-init-rotate trans-matrix-rotate trans-matrix-scale
   trans-matrix-rotate trans-matrix-invert trans-matrix-multiply
   trans-matrix-distance transform-point

   ;; xlib/xlib-image-interface

   xlib-image-context

   ;; xcb

   create-xcb-surface xcb-surface-set-size

   ;; freetype

   freetype-font-face ft-scaled-font-lock-face ft-scaled-font-unlock-face
   with-ft-scaled-face-locked
   ))

(defpackage :scenic.style
  (:use :cl)
  (:export :stylesheet
           :*stylesheets*
           :style-rule
           :parse-style-rule-selector
           :make-style-rule
           :find-stylesheet
           :add-rule
           :defstylesheet
           :defstyle-rule
           :in-stylesheeet
           :apply-style-property
           :apply-stylesheet
           :find-widget
           :find-widgets
           :find-widget-by-name
           :map-widgets
           :padding
           :expand-padding-spec
           :padding-left
           :padding-right
           :padding-top
           :padding-bottom
           :border
           :border-width
           :border-style
           :border-color
           :text
           :text-family
           :text-slant
           :text-size
           :text-color
           :linear-gradient
           :color
           :expand-style-spec
           :merge-style-spec))

(defpackage :scenic.renderer
  (:use :cl :scenic.style)
  (:export :renderer
           :defrenderer
           :set-renderer
           :*renderer*
           :with-renderer
           :find-renderer
           :gtk-renderer
           :standard-renderer
           :wnd7-renderer
           :material-renderer
           :rect-renderer))

(defpackage :scenic.animation
  (:use :cl)
  (:export
   :animation
   :scenic-animation
   :animation-state
   :animation-target
   :animation-duration
   :animation-speed
   :animation-easing
   :basic-animation
   :variant-animation
   :property-animation
   :animation-group
   :sequential-animation-group
   :parallel-animation-group

   ;; Play API
   :start-animation
   :start
   :stop
   :pause
   :resume
   :set-current-time
   :in-sequence
   :in-parallel
   
   ;; Animations
   :fade-animation
   :fade
   :fadeout
   :fadein
   :translation-animation
   :translate
   :resize-animation
   :resize
   :rotate-animation
   :rotate
   :property-animation
   
   ;; Easing
   :linear-tween
   :ease-in-quad
   :ease-out-quad
   :ease-in-out-quad
   :ease-in-sin
   :ease-out-sin
   :ease-in-out-sin
   ))
