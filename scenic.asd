(asdf:defsystem #:scenic-core
  :serial t
  :depends-on (#:gzip-stream
               #:bordeaux-threads
               #:mbrezu-utils
               #:trivial-backtrace
               #:cl-colors
               #:css-selectors
               #:cl-arrows
               #:access
               #:closer-mop
               #:cl-rsvg2
               #:chanl)
  :components ((:file "packages")
               (:module src
                        :components
                        ((:file "draw")
                         (:file "utils")
                         (:file "resources")
                         (:file "events")
                         (:file "classes")
                         (:file "textbox")
                         (:file "containers")
                         (:file "grid")
                         (:file "scene")
                         (:file "decorators")
                         (:file "buttons")
                         (:file "scroll")
                         (:file "styles")
                         (:file "renderer")
                         (:file "animation")
                         (:file "mop")
                         (:file "helpers")
                         (:file "tasks")
                         (:file "stepping")
                         (:file "scenic")
                         (:module :nui
                                  :components ((:file "nui")
                                               (:file "icon")
                                               (:file "layout")
                                               (:file "rbox")
                                               (:file "button")
                                               (:file "checkbox")
                                               (:file "scrollbar")
                                               (:file "listview")
                                               (:file "tabview")
                                               (:file "textbox")
                                               (:file "expandable")
                                               (:file "toolbar")
                                               (:file "popup")
                                               (:file "dropdown")
                                               (:file "splitter")
                                               (:file "drag")))
                         )))
  :serial t)

(asdf:defsystem #:scenic-sdl2
  :serial t
  :depends-on (#:scenic-core
               #:sdl2kit
               #:cl-cairo2
               #:cl-rsvg2
               #:sdl2-image)
  :components ((:module src
                        :components
                        ((:file "sprite")
                         (:file "draw")
                         (:file "draw-cairo")
                         (:file "sdl2-ffi")
                         (:file "sdl2"))))
  :serial t)

(defmethod asdf:perform :before ((op asdf:prepare-op) (c (eql (asdf:find-system ':scenic-sdl2))))
  (pushnew :scenic-sdl2 *features*))

(defmethod asdf:perform :before ((op asdf:load-op) (c (eql (asdf:find-system ':scenic-sdl2))))
  (pushnew :scenic-sdl2 *features*))

(asdf:defsystem #:scenic-sdl
  :serial t
  :depends-on (#:scenic-core
               #:lispbuilder-sdl
               #:cl-cairo2
               #:cl-rsvg2)
  :components ((:module src
                        :components
                        ((:file "draw")
                         (:file "draw-cairo")
                         (:file "sdl"))))
  :serial t)

(asdf:defsystem #:scenic
  :depends-on (#:scenic-sdl2))

(defmethod asdf:perform :before ((op asdf:prepare-op) (c (eql (asdf:find-system ':scenic))))
  (pushnew :scenic-sdl2 *features*))

(defmethod asdf:perform :before ((op asdf:load-op) (c (eql (asdf:find-system ':scenic))))
  (pushnew :scenic-sdl2 *features*))

(ql:quickload :cffi)
(push #p"/usr/local/lib/" cffi:*foreign-library-directories*)

(asdf:defsystem #:scenic-nvg
  :serial t
  :depends-on (#:scenic-core
               #:bodge-nanovg
               #:sdl2kit)
  :components ((:module src
                        :components
                        ((:file "draw")
                         (:file "draw-nvg")
                         (:file "sdl2-ffi")
                         (:file "sdl2-nvg"))))
  :serial t)
