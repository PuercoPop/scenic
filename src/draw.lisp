(in-package :scenic.draw)

(defstruct alpha-color
  color
  alpha)

(defstruct linear-gradient
  direction color-stops extent)

(defun linear-gradient (x0 y0 x1 y1 &rest color-stops)
  (let ((pat (cairo:create-linear-pattern
              x0 y0 x1 y1)))
    (cairo:pattern-add-color-stop pat 0 from-color)
    (cairo:pattern-add-color-stop pat 1 to-color)
    (cairo:set-source pat)
    pat))

(defun parse-linear-gradient (gradient-spec)
  (destructuring-bind (_ direction &rest color-stops) gradient-spec
    (declare (ignore _))
    (make-linear-gradient :direction direction :color-stops color-stops)))

(defun apply-linear-gradient (linear-gradient)
  (with-slots (direction color-stops extent) linear-gradient
    (let* ((dir (case direction
                  (:right (list 0 0 extent 0))
                  (:left (list extent 0 0 0))
                  (:bottom (list 0 0 0 extent))
                  (:top (list 0 extent 0 0))
                  (t direction)))
           (pat (apply #'cairo:create-linear-pattern
                       dir)))
      (let ((delta (/ 1 (1- (length color-stops)))))
        (loop
           :for offset := 0 :then (+ offset delta)
           :for color-stop :in color-stops
           :do
           (cairo:pattern-add-color-stop pat offset (parse-color-or-pattern color-stop))))
      (cairo:set-source pat)
      pat)))

(defmethod brighten ((color cl-colors:rgb) delta)
  #+nil(let ((hsv (cl-colors:rgb-to-hsv color)))
    (cl-colors:hsv-to-rgb
     (cl-colors:hsv (cl-colors:hsv-hue hsv)
                    (cl-colors:hsv-saturation hsv)
                    (+ (cl-colors:hsv-value hsv) delta))))
  color
  )


(defmethod brighten ((color linear-gradient) delta)
  (setf (linear-gradient-color-stops color)
        (mapcar (lambda (c)
                  (brighten c delta))
                (linear-gradient-color-stops color))))

(defmethod brighten (color delta)
  (brighten (parse-color-or-pattern color) delta))

(defun parse-color-or-pattern (source)
  (cond
    ((and (listp source)
          (eql (first source) :linear-gradient))
     (apply #'make-linear-gradient (rest source)))
    ((and (listp source)
          (eql (first source) :rgb))
     (apply #'cl-colors:rgb (rest source)))
    ((and (listp source)
          (eql (first source) :rgba))
     (destructuring-bind (r g b a) (rest source)
       (make-alpha-color :color (cl-colors:rgb r g b)
                         :alpha a)))
    ((stringp source)
     (let ((color-symbol (intern (string-upcase (format nil "+~A+" source))
                                 :cl-colors)))
       (if (boundp color-symbol)
           (symbol-value color-symbol)
           (cl-colors:parse-hex-rgb source))))
    ((typep source 'cl-colors:rgb)
     source)
    (t (error "Cannot parse color: ~A" source))))

(defgeneric set-fill-extent (color extent))
(defmethod set-fill-extent (color extent))
(defmethod set-fill-extent ((color linear-gradient) extent)
  (setf (linear-gradient-extent color) extent))              
  
(defgeneric set-color (color-or-pattern))

(defmethod set-color ((gradient linear-gradient))
  (apply-linear-gradient gradient))

(defmethod set-color ((color string))
  (set-color (parse-color-or-pattern color)))

(defun parse-unit ()
  )

(defun parse-dimentions-unit (dimentions)
  "Parse/check dimentions. Dimentions can be auto, ext, px, %

- auto: auto layout in grid
- ext: proportion
- px: pixels
- %: percent"
  )

(defun draw-svg (filepath x y w h)
  (or (probe-file filepath)
      (error "File not found: ~A" filepath))      
  (cairo:translate x y)
  (rsvg2:with-handle-from-file (svg filepath)
    (multiple-value-bind (width height)
        (rsvg2::handle-get-dimension-values svg)
        (cairo:scale (/ w width) (/ h height))
        (rsvg2:draw-svg svg)
        (cairo:scale (/ 1 (/ w width)) (/ 1 (/ h height)))
        (cairo:translate (- 0 x) (- 0 y)))))
