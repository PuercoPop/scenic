;;-- * Dropdown

(in-package :scenic.nui)

;;-- ** Definition

(defclass dropdown (nui-widget scenic:clickable)
  ((status :initform :closed
           :accessor dropdown-status)
   (dropdown-function :initarg :dropdown
                      :accessor dropdown-function
                      :type function)
   (%new-window :initarg :new-window
                :accessor %new-window
                :initform nil
                :type boolean)   
   (%popup :accessor %dropdown-popup
           )))

;;-- ** Initialization

(defmethod initialize-instance :after ((dropdown dropdown) &rest initargs)
  (add-event-handler dropdown :click :bubble
                     (lambda (dropdown event)
                       (ecase (dropdown-status dropdown)
                         (:closed (open-dropdown dropdown))
                         (:opened (close-dropdown dropdown))))))

;;-- ** Painting

(defmethod renderer-paint (renderer (dropdown dropdown))
  (scenic-utils::draw-rect (layout-left dropdown)
                           (layout-top dropdown)
                           (layout-width dropdown)
                           (layout-height dropdown)
                           :fill-color cl-colors:+lightgray+
                           :stroke-color nil))

;;-- ** Layout

(defmethod renderer-measure (renderer (dropdown dropdown) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child dropdown) available-width available-height)
    (set-measured dropdown width height)
    (values width height)))

(defmethod renderer-layout (renderer (dropdown dropdown) left top width height)
  (layout (child dropdown) left top width height)
  (set-layout dropdown left top width height))

;;-- ** Events

(defun open-dropdown (dropdown)
  (when (eql (dropdown-status dropdown) :closed)
    (let ((dropdown-widget (funcall (dropdown-function dropdown) dropdown))
          (new-window-p (%new-window dropdown)))
      (multiple-value-bind (wnd-x wnd-y)
          (scenic::window-position (scenic::focused-window))
        (when (not new-window-p)
          (setf wnd-x 0 wnd-y 0))
        (let ((popup
               (scenic::popup dropdown-widget
                      :position (cons (+ wnd-x (layout-left dropdown))
                                      (+ wnd-y
                                         (layout-top dropdown)
                                         (layout-height dropdown)
                                         0))
                      :sdl-window new-window-p)))
          (setf (%dropdown-popup dropdown) popup)
          (setf (dropdown-status dropdown) :opened))))))

(defun close-dropdown (dropdown)
  (when (eql (dropdown-status dropdown) :opened)
    (let ((popup (%dropdown-popup dropdown)))
      (scenic::close-popup popup)
      (setf (%dropdown-popup dropdown) nil)
      (setf (dropdown-status dropdown) :closed))))

;;-- ** Select widget

(defclass select (dropdown)
  ((selected-option :initarg :selected-option
                    :initform nil
                    :accessor selected-option)
   (selected-option-view :initarg :selected-option-view
                         :initform nil)
   (empty-option-view :initarg :empty-option-view
                      :initform nil)
   (placeholder :initarg :placeholder
                :initform nil
                :accessor select-placeholder)
   (options :initarg :options
            :initform nil
            :accessor select-options)))

(defun selected-option-value (select)
  (anaphora:awhen (selected-option select)
    (value anaphora:it)))  

(defun default-selected-option-view (select)
  (uniform-padding 5
                   (flow-row (:spacing 5)
                     (selected-option select)
                     (sizer (fa-icon "caret_down")
                            :width 10
                            :height 16))))

(defmethod selected-option-view ((select select))
  (with-slots (selected-option) select
    (or (anaphora:aand (slot-value select 'selected-option-view)
                       (funcall anaphora:it select))
        (default-selected-option-view select))))

(defmethod empty-option-view ((select select))
  (or (anaphora:aand (slot-value select 'empty-option-view)
                     (funcall anaphora:it select))
      (default-empty-option-view select)))

(defun default-empty-option-view (select)
  (uniform-padding 5
                   (flow-row (:spacing 5)
                     (label (or (select-placeholder select)
                                "Select an option ..."))
                     (sizer (fa-icon "caret_down")
                            :width 10
                            :height 16))))

(defmethod initialize-instance :after ((select select) &rest initargs)
  (let ((child (if (selected-option select)
                   (selected-option-view select) 
                   (empty-option-view select))))
    ;; Don't use (setf child). Avoid invalidate and layout calls
    (setf (slot-value select 'child) child)
    (setf (scenic:parent child) select)))

(defmethod select-option ((select select) option)
  (setf (selected-option select) (child option))
  (close-dropdown select)
  (setf (child select)
        (if (selected-option select)
            (selected-option-view select)
            (empty-option-view select))))

(defclass select-options-view (listview)
  ()
  (:metaclass scenic::component-class))

(defmethod scenic::build ((view select-options-view))
  (mapcan (lambda (li)
            (setf (owner li) view))
          (list-items view))
  (setf (slot-value view '%content)
        (background (fill-color view)
                    (vertical-pack 0 (mapcar (constantly :auto)
                                             (list-items view))
                                   (list-items view)))))

(defun select-dropdown (select)
  (adjuster
   (make-instance 'select-options-view
                  :on-item-selected (lambda (option) (select-option select option))
                  :items
                  (select-options select))))

(defmethod dropdown-function ((select select))
  #'select-dropdown)

(defclass select-option (listitem)
  ((value :initarg :value
          :accessor value
          :initform nil)))

(defmacro soption (options child)
  `(make-instance 'select-option :child ,child ,@options))

(defclass select-options-separator (separation-line)
  ())

(defun ssep ()
  (make-instance 'select-option-separator))

(defmethod selectable-option-p (x)
  "Default behaviour is that select options are selectable"
  t)

(defmethod selectable-option-p ((x select-options-separator))
  nil)

;;-- ** API

(defun dropdown (child &key new-window dropdown)
  (make-instance 'dropdown :child child
                 :dropdown dropdown
                 :new-window new-window))

(defmacro select (args &body options)
  `(make-instance 'select
                  ,@args
                  :options (list ,@options)))

;;-- ** Examples

(defun dropdown-ex-1 ()
  (flet ((dropdown (label)
           (dropdown
                   (uniform-padding 5
                                    (flow-row (:spacing 5)
                                      (label label)
                                      (sizer (fa-icon "caret_down")
                                             :width 10
                                             :height 16)))
                   :dropdown
                   (lambda (dropdown)
                     #+nil(sizer 
                      (background cl-colors:+white+ 
                                  (uniform-padding
                                   10
                                   (label "Dropdown")))
                      :max-height 100 :max-width 100)
                     (scenic::adjuster
                      (background cl-colors:+white+ 
                                  (uniform-padding
                                   10
                                   (label "Dropdown"))))
                     ))))
        (run-component (stack (background cl-colors:+white+ (filler))
                              (scenic::adjuster
                               (flow-col (:spacing 2)
                                 (dropdown "Foo")
                                 (dropdown "Bar")
                                 (dropdown "Baz")))))))

(defun dropdown-ex-2 ()
  (let ((dropdown (dropdown
                   (uniform-padding 5
                                    (flow-row (:spacing 2)
                                      (label "Foo")
                                      (sizer (fa-icon "caret_down")
                                             :width 10
                                             :height 16)))
                   :new-window t
                   :dropdown
                   (lambda (dropdown)
                     #+nil(sizer 
                      (background cl-colors:+white+ 
                                  (uniform-padding
                                   10
                                   (label "Dropdown")))
                      :max-height 100 :max-width 100)
                     (scenic::adjuster
                      (background cl-colors:+white+ 
                                  (uniform-padding
                                   10
                                   (label "Dropdown"))))
                     ))))
        (run-component (stack (background cl-colors:+red+ (filler))
                              (scenic::adjuster dropdown)))))

;; (dropdown-ex-1)
;; (dropdown-ex-2)

(defun select-ex-1 ()
  (flet ((option (val label)
           (soption (:value val
                           :hover-color cl-colors:+lightblue+
                           :selected-color cl-colors:+blue+
                           :padding-bottom 3
                           :padding-top 3
                           :padding-left 5
                           :padding-right 5)
                   (label label))))
      (let ((select (select ()
                      (option :hello "Hello")
                      (option :world "World"))))
        (run-component (stack (background cl-colors:+red+ (filler))
                              (scenic::adjuster select))))))

;; (select-ex-1)
