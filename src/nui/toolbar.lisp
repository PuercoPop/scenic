;;-- * Toolbar

(in-package :scenic.nui)

(defun png-toolbar-ex ()
  (stack
   (background cl-colors:+gray+
               (filler))
   (uniform-padding
    10
    (simple-horizontal-pack
     4
     (loop for i from 1 to 10
        collect
          (sizer (stack
                  (background (list 0.8 0.8 0.8)
                              (filler))
                  (button
                   (uniform-padding 5 (image "icons/arrow_in.png"))))
                 :max-width 30
                 :min-width 30
                 :max-height 30
                 :max-width 30))))))


(defun svg-toolbar-ex ()
  (let ((icons (list "dropbox" "desktop" "dashboard" "comment" "cut" "compass" "copy")))
    (stack
     (background cl-colors:+gray+
                 (filler))
     (uniform-padding
      10
      (simple-horizontal-pack
       4
       (loop for icon in icons
          collect
            (sizer (stack
                    (background (list 0.8 0.8 0.8)
                                (filler))
                    (nui-button
                     (uniform-padding 5 (fa-icon icon))))
                   :max-width 30
                   :min-width 30
                   :max-height 30
                   :max-width 30)))))))

;;-- ** Examples

;; (run-component (png-toolbar-ex))
;; (run-component (svg-toolbar-ex))
