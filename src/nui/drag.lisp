(in-package :scenic.nui)

(defun make-pos-draggable (instance)
  (let ((dragging nil)
        (inside-x)
        (inside-y))
    (add-event-handler instance :mouse-button-down :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (setf dragging t)
                           (drag-started instance)
                           (setf inside-x (- (mouse-x event) (scenic::left instance)))
                           (setf inside-y (- (mouse-y event) (scenic::top instance)))
                           (scenic::capture-mouse instance)
                           (scenic::invalidate instance)
                           (scenic::invalidate (scenic:parent instance)))))
    (add-event-handler instance :mouse-button-up :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (setf dragging nil)
                           (drag-stopped instance)
                           (release-mouse instance)
                           (scenic::invalidate instance)
                           (scenic::invalidate (scenic:parent instance)))))
    (add-event-handler instance :mouse-move :bubble
                       (lambda (instance event)
                         (when dragging
                           (setf (scenic::left instance) (max 0 (- (mouse-x event) inside-x)))
                           (setf (scenic::top instance) (max 0 (- (mouse-y event) inside-y)))
                           (scenic::mark-for-layout (scenic:parent instance))
                           (scenic::invalidate (scenic:parent instance))
                           (scenic::invalidate instance)

                           ;; This is VERY bad: why is it not working without this? (invalidations above are not taking effect)
                                        ;(scenic::render-scene (scenic::get-scene instance) t))
                           (scenic::invalidate-scene (scenic::get-scene instance))
                           )))))

;; Dragging example

(defclass shadow-container (scenic::container1)
  ((shadow-enabled :accessor shadow-enabled-p
                   :initform nil
                   :initarg :shadow-enabled)))

(defun shadow* (child &rest args)
  (apply #'make-instance 'shadow-container :child child args))

(defmethod scenic:layout ((w shadow-container) left top width height)
  (layout (child w) left top width height)
  (set-layout w left top (measured-width w) (measured-height w)))

(defmethod scenic::paint :before ((w shadow-container))
  (when (shadow-enabled-p w)
    ;; Paint a shadow
    (let ((offset 2))
      (scenic-utils::draw-rect (+ (layout-left (child w)) offset)
                               (+ (layout-top (child w)) offset)
                               (layout-width (child w))
                               (layout-height (child w))
                               :stroke-color nil
                               :fill-color (scenic.draw::make-alpha-color
                                            :color cl-colors:+gray+
                                            :alpha 0.3)))))

(defmethod drag-started (w))

(defmethod drag-started ((w shadow-container))
  (setf (shadow-enabled-p w) t))

(defmethod drag-started ((w scenic::placer))
  (drag-started (child w)))

(defmethod drag-stopped (w))
(defmethod drag-stopped ((w shadow-container))
  (setf (shadow-enabled-p w) nil))
(defmethod drag-stopped ((w scenic::placer))
  (drag-stopped (child w)))

(defun make-draggable (instance)
  (let ((dragging nil)
        (inside-x)
        (inside-y))
    (add-event-handler instance :mouse-button-down :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (setf dragging t)
                           (drag-started instance)
                           (setf inside-x (- (mouse-x event) (layout-left instance)))
                           (setf inside-y (- (mouse-y event) (layout-top instance)))
                           (scenic::capture-mouse instance)
                           (scenic::invalidate instance)
                           (scenic::invalidate (scenic:parent instance)))))
    (add-event-handler instance :mouse-button-up :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (drag-stopped instance)
                           (setf dragging nil)
                           (release-mouse instance)
                           (scenic::invalidate instance)
                           (scenic::invalidate (scenic:parent instance)))))
    (add-event-handler instance :mouse-move :bubble
                       (lambda (instance event)
                         (when dragging
                           (setf (layout-left instance) (max 0 (- (mouse-x event) inside-x)))
                           (setf (layout-top instance) (max 0 (- (mouse-y event) inside-y)))
                           (scenic::mark-for-layout (scenic:parent instance))
                           (scenic::invalidate (scenic:parent instance)))))))

(defun drag-ex-1 ()

  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (scenic::placer 0 0 :bind rect-2
                                     (shadow*
                                      (sizer
                                       (background  (scenic.draw:parse-color-or-pattern
                                                     '(:rgba 1 0 0 1))
                                                    (filler))
                                       :width 50 :height 50)))

                 (scenic::placer 0 0 :bind rect-3
                                     (shadow*
                                      (sizer
                                       (background (scenic.draw:parse-color-or-pattern
                                                    '(:rgba 1 1 0 1))
                                                   (filler))
                                       :width 200 :height 100)))
                 (scenic::placer 0 0 :bind rect-1 (shadow*
                                                       (sizer (background
                                                               (scenic.draw:parse-color-or-pattern
                                                                '(:rgba 0 0 0 0.4))
                                                               (filler))
                                                              :width 100 :height 100)))
                 )))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        (make-pos-draggable rect-1)
        (make-pos-draggable rect-2)
        (make-pos-draggable rect-3)
        (scenic:run-scene scene :window-options '(:resizable t))

        ))))

;; Dragging with placer


(defun drag-ex-2 ()
  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (scenic::placer 0 0 :bind rect-1
                                     (sizer
                                      (background (scenic.draw:parse-color-or-pattern
                                                   '(:rgba 1 0 0 1))
                                                  (uniform-padding 50
                                                                   (label "Drag me")))
                                      :height 100 :width 100))
                 )))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        (make-pos-draggable rect-1)
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (drag-ex-1)
;; (drag-ex-2)
