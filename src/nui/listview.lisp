(in-package :scenic.nui)

;; * List view

;; ** Definition
(scenic::defcomponent listview ()
  ((selected-item :initarg :selected-item
                  :accessor selected-item
                  :state t
                  :on-change (:invalidate))
   (fill-color :initarg :fill-color
               :accessor fill-color
               :initform cl-colors:+white+)
   (items :initarg :items
          :accessor list-items
          :on-change (:invalidate :layout :rebuild))
   (item-selected-callback :initarg :on-item-selected
                           :accessor item-selected-callback
                           :initform nil)
   (%content :child t :initform nil)))

;; Initialization/creation

(defmethod scenic::paint ((listview listview)))

(defmethod scenic::build ((listview listview))
  (mapcan (lambda (li)
            (setf (owner li) listview))
          (list-items listview))
  (setf (slot-value listview '%content)
        (scroll-view-auto
         (background (fill-color listview)
                     (vertical-pack 0 (mapcar (constantly :auto)
                                             (list-items listview))
                                   (list-items listview)))
         :always-vertical-scrollbar nil
         :always-horizontal-scrollbar nil)))

(defmethod scenic:measure ((component listview) available-width available-height)
  (multiple-value-bind (width height)
      (scenic:measure (slot-value component '%content) available-width available-height)
    (set-measured component width height)))

(defmethod scenic::layout ((object listview) left top width height)
  (scenic::layout (slot-value object '%content)
                  left
                  top
                  width
                  height)
  (scenic::set-layout object left top width height))

(defun item-selected (lv li)
  (loop :for item :in (list-items lv)
     :do
     (setf (slot-value item 'selected) nil)
     (setf (fill-color item) (ascendant-fill-color li))
     (scenic:invalidate item))
  (setf (slot-value li 'selected) t)
  (scenic:invalidate li)
  (when (item-selected-callback lv)
    (funcall (item-selected-callback lv) li)))

;; * List box

(scenic::defcomponent listbox (listview)
  ((key :initarg :key
        :accessor list-key
        :initform #'identity)
   (test :initarg :test
         :accessor list-test
         :initform #'eql)))

(defclass listitem (rbox-1)
  ((owner :initform nil
          :initarg :owner
          :accessor owner
          :documentation "listview owner")
   (selected :initform nil
             :accessor selected
             :initarg :selected)
   (selectable :initform t
               :accessor selectable-p
               :initarg :selectable)
   (selected-color :initform nil
                   :accessor selected-color
                   :initarg :selected-color)
   (hover-color :initform nil
                :accessor hover-color
                :initarg :hover-color)))

(defmethod scenic:paint :before ((li listitem))
  (when (and (selected li) (selected-color li))
    (setf (fill-color li) (selected-color li))))

(defun li-mouse-enter (li event)
  (when (hover-color li)
    (setf (fill-color li) (hover-color li))
    (scenic:invalidate li)))

(defun li-mouse-leave (li event)
  (when (hover-color li)
    (setf (fill-color li) (ascendant-fill-color li))
    (scenic:invalidate (scenic:parent li))
    ))

(defun li-mouse-click (li event)
  (setf (selected li) t))
 
(defmethod (setf selected) :after (new-value (li listitem))
  (if new-value
      (item-selected (owner li) li)
      (setf (fill-color li) (ascendant-fill-color li))))

(defmethod initialize-instance :after ((li listitem) &rest initargs)
  (when (selectable-p li)
    (scenic:add-event-handler li :mouse-enter :bubble #'li-mouse-enter)
    (scenic:add-event-handler li :mouse-leave :bubble #'li-mouse-leave)
    (scenic:add-event-handler li :mouse-button-down :bubble #'li-mouse-click)))

(defclass listitem-separator (listitem)
  ()
  (:default-initargs :selectable nil))

(defmethod initialize-instance :after ((sep listitem-separator) &rest initargs)
  (setf (child sep) (separation-line)))

(defun li-sep (&rest args)
  (apply #'make-instance 'listitem-separator args))
  
;; * API

(defmacro listview (args &body items)
  `(make-instance 'listview
                  ,@args
                  :items (list ,@items)))

(defmacro li (options child)
  `(make-instance 'listitem :child ,child ,@options))

(export '(listview li listbox))

;; * Examples

(defun listview-ex-1 ()
  (flet ((li* (child)
           (li (:hover-color cl-colors:+lightcyan+
                             :selected-color cl-colors:+cyan+)
               child)))
    (let ((list (listview ()
                  (li* (uniform-padding 10 (label "Hello")))
                  (li* (uniform-padding 10 (label "Bye asdf sdf af")))
                  (li* (uniform-padding 10 (label "ASDFAF"))))))
      (run-component (horizontal-pack* 0 '(:auto)
                                      list)))))

;; (listview-ex-1)

(defun listview-ex-2 ()
  (flet ((li* (child)
           (li (:hover-color cl-colors:+lightcyan+
                             :selected-color cl-colors:+cyan+)
               child)))
    (let ((list (listview ()
                  (li* (uniform-padding 10 (label "Hello")))
                  (li* (uniform-padding 10 (label "Bye asdf sdf af")))
                  (li-sep)
                  (li* (uniform-padding 10 (label "ASDFAF"))))))
      (run-component (horizontal-pack* 0 '(:auto)
                                      list)))))

;; (listview-ex-2)
