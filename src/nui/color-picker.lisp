(scenic::defcomponent simple-color-picker (scenic::container-1)
  ((picked-color :initarg :color
                 :accessor picked-color)))

(defmethod scenic::build ((picker simple-color-picker))
  (setf (child picker)))        
                 
