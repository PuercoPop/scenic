;;-- * Images and icons

(in-package :scenic.nui)

;;-- ** Examples

(defun png-icon ()
  (stack
   (background (list 1.0 1.0 1.0)
               (filler))
   (uniform-padding 10
                    (sizer (stack
                            (background (list 0.8 0.8 0.8)
                                        (filler))
                            (image "icons/arrow_in.png"))
                           :max-width 16
                           :min-width 16
                           :max-height 16
                           :max-width 16))))

(defun png-icon-2 ()
  (stack
   (background (list 1.0 1.0 1.0)
               (filler))
   (uniform-padding 10
                    (stack
                     (background (list 0.8 0.8 0.8)
                                 (filler))
                     (image "icons/arrow_in.png")))))

(defun lisp-image-ex ()
  (run-component
   (stack
   (background (list 1.0 1.0 1.0)
               (uniform-padding 10
                    (image "icons/lisplogo_fancy_256.png"))))))

;; (run-component (png-icon))
;; (run-component (png-icon-2))
;; (lisp-image-ex)
