(in-package :scenic.nui)

(defclass resizable (scenic::container1)
  ((initial-width :initform nil
                  :initarg :initial-width
                  :accessor initial-width)
   (initial-height :initform nil :initarg
                   :initial-height
                   :accessor initial-height)))

(defmethod measure ((object resizable) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object) available-width available-height)
    (set-measured object width height)))

(defclass splitter (scenic:widget scenic::orientable)
  ((initial-x :initarg :initial-x :initform 0.5 :accessor initial-x)
   (initial-y :initarg :initial-y :initform 0.5 :accessor initial-y))
  (:default-initargs :orientation :vertical))

(defmethod layout ((object splitter) left top width height)
  (set-layout object
              (or (layout-left object) left)
              (or (layout-top object) top)
              width height))

(defmethod measure ((object splitter) available-width available-height)
  (ecase (scenic::orientation object)
    (:vertical
     (call-next-method object 4 0))
    (:horizontal
     (call-next-method object 0 4))))

(defmethod paint ((object splitter))
  (ecase (scenic:orientation object)
    (:vertical
     (scenic.draw:set-line-width (layout-width object))

     (scenic.draw:set-source-color cl-colors:+gray+)
     (scenic.draw:move-to (+ (layout-left object) 0) (layout-top object))
     (scenic.draw:line-to (+ (layout-left object) 0)
                        (+ (layout-top object) (layout-height object)))
     (scenic.draw:stroke))
    (:horizontal
     (scenic.draw:set-line-width (layout-height object))

     (scenic.draw:set-source-color cl-colors:+gray+)
     (scenic.draw:move-to (+ (layout-left object) 0) (layout-top object))
     (scenic.draw:line-to (+ (layout-left object) (layout-width object))
                        (layout-top object) (layout-height object))
     (scenic.draw:stroke))
    ))

(defun splitter-drag-area (splitter)
  (let ((offset 2))
    (scenic::ifhorizontal splitter
                          (values (layout-left splitter)
                                  (max (- (layout-top splitter) offset) 0)
                                  (layout-width splitter)
                                  (+ (layout-height splitter) offset))
                          (values (max (- (layout-left splitter) offset) 0)
                                  (layout-top splitter)
                                  (+ (layout-width splitter) offset)
                                  (layout-height splitter)))))

(defmethod scenic::in-widget (x y (widget splitter))
  (multiple-value-bind (left top width height)
      (splitter-drag-area widget)
    (scenic::in-rectangle x y
                          left top width height)))

(defparameter *out* *standard-output*)

(defmethod initialize-instance :after ((instance splitter) &rest initargs)
  (declare (ignore initargs))
  (let ((dragging nil))
    (add-event-handler instance :mouse-button-down :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (setf dragging t)
                           (scenic::capture-mouse instance))))
    (add-event-handler instance :mouse-button-up :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (scenic::set-cursor :arrow)
                           (setf dragging nil)
                           (release-mouse instance))))
    (add-event-handler instance :mouse-enter :bubble
                       (lambda (instance event)
                         (scenic::set-cursor :sizewe)))
    (add-event-handler instance :mouse-leave :bubble
                       (lambda (instance event)
                         (scenic::set-cursor :arrow)))
    (add-event-handler instance :mouse-move :bubble
                       (lambda (instance event)
                         (when dragging
                           #+nil(format *out* "Dragging: ~A from: ~A to: ~A~%"
                                   instance
                                   (layout-left instance)
                                   (mouse-x event)
                                   )
                           (scenic::ifhorizontal instance
                                                 (setf (layout-top instance) (- (mouse-y event)))
                                                 (setf (layout-left instance) (mouse-x event)))
                           (setf (dragging (scenic:parent instance)) t)
                           ;; Calling invalidate and then mark-for-layout, in that order, is very important, otherwise the splitted components are not drawn correctly
                           (scenic::invalidate (scenic:parent instance))
                           (scenic::mark-for-layout (scenic:parent instance))
                           
                           )))))

(defun splitter (&rest initargs)
  (apply #'make-instance 'splitter initargs))

(defclass splitter-container (scenic:container scenic:orientable)
  ((dragging :initform nil :accessor dragging))
  (:default-initargs :orientation :horizontal))

(defmethod measure ((object splitter-container) available-width available-height)
  (let ((splitters (loop
                      :for child :in (children object)
                      :when (typep child 'splitter)
                      :collect child))
        (in-between (split-sequence:split-sequence-if
                     (lambda (x)
                       (typep x 'splitter))
                     (children object))))
    (setf (dragging object) nil)
    (flet ((measure-children (children available-width available-height)
             (mapcar #'(lambda (widget)
                         (multiple-value-list
                          (measure widget available-width available-height)))
                     children)))
      (loop
         with running-left := 0
         for splitter in splitters
         for splitter-left := (or (layout-left splitter)
                                  (round (* (initial-x splitter) available-width)))
         for before-splitter in in-between
         do
           (measure-children before-splitter
                             (- splitter-left
                                running-left)
                             available-height)
           (setf running-left (+ splitter-left
                                 (or (layout-width splitter) 4)))
         finally (measure-children (car (last in-between))
                                   (- available-width running-left)
                                   available-height))
      ;; Return the container measures
      (values available-width available-height))))

(defmethod layout ((object splitter-container) left top width height)
  (ecase (scenic:orientation object)
      (:vertical
       (let ((running-top top))
         (dolist (widget (children object))
           (when (not (and (dragging object)
                           (typep widget 'splitter)))
             (layout widget left running-top width (measured-height widget)))
           (incf running-top (measured-height widget)))))
      (:horizontal
       (let ((running-left left))
         (dolist (widget (children object))
           (when (not (and (dragging object)
                           (typep widget 'splitter)))
             (layout widget running-left top (measured-width widget) height))
           (incf running-left (measured-width widget))))))
    (call-next-method object left top width height))

(defun splitter-container (orientation &rest children)
  (make-instance 'splitter-container
                 :children children
                 :orientation orientation))

(defun splitter-ex-1 ()
  (let ((comp (splitter-container :horizontal
                                  (sizer
                                   (background cl-colors:+red+ (filler))
                                   :min-width 100
                                   :max-width 200)
                                  (splitter)
                                  (sizer
                                   (background cl-colors:+blue+ (filler))))))
    (run-component comp)))

;; (splitter-ex-1)

(defun splitter-ex-2 ()
  (let ((comp (splitter-container :horizontal
                                  (scenic::expander
                                   (background cl-colors:+red+ (filler)))
                                  (splitter)
                                  (scenic::expander
                                   (background cl-colors:+blue+ (filler))))))
    (run-component comp)))

;; (splitter-ex-2)

;; Fix:

#+fix(flet ((dropdown (label)
              (dropdown
               (uniform-padding 5
                                (flow-row (:spacing 5)
                                  (label label)
                                  (sizer (fa-icon "caret_down")
                                         :width 10
                                         :height 16)))
               :dropdown
               (lambda (dropdown)
                 #+nil(sizer
                       (background cl-colors:+white+
                                   (uniform-padding
                                    10
                                    (label "Dropdown")))
                       :max-height 100 :max-width 100)
                 (scenic::adjuster
                  (background cl-colors:+white+
                              (uniform-padding
                               10
                               (label "Dropdown"))))
                 ))))
       (run-component (stack (background cl-colors:+red+ (filler))
                             (splitter-container :horizontal
                                                 (sizer
                                                  (flow-col (:spacing 2)
                                                    (dropdown "Foo")
                                                    (dropdown "Bar")
                                                    (dropdown "Baz"))
                                                  :min-width 100
                                                  :max-width 200)
                                                 (splitter)
                                                 (sizer
                                                  (flow-col (:spacing 2)
                                                    (dropdown "Foo")
                                                    (dropdown "Bar")
                                                    (dropdown "Baz")))))))

;; (run-component
;;  (sizer (splitter-container
;;                 :horizontal
;;                 (scenic::expander
;;                  (background cl-colors:+white+
;;                              (flow-col (:spacing 2)
;;                                (label "Foo")
;;                                (label "Bar")
;;                                (label "Baz"))))
;;                 (splitter)
;;                 (scenic::expander
;;                   (background cl-colors:+white+
;;                               (flow-col (:spacing 2)
;;                                 (label "Foo")
;;                                 (label "Bar")
;;                                 (label "Baz")))))
;;         :width 500
;;         :height 500))


(defclass splitter-container-2 (scenic:widget scenic:orientable)
  ((dragging :initform nil :accessor dragging)
   (splitter-position :initarg :splitter-position
                      :initform 0.5
                      :accessor splitter-position)
   (child1 :initarg :child1
           :accessor child1)
   (splitter :initarg :splitter
             :accessor container-splitter)
   (child2 :initarg :child2
           :accessor child2))
  (:default-initargs :orientation :horizontal))

(defmethod children ((object splitter-container-2))
  (with-slots (child1 splitter child2) object
    (list child1 splitter child2)))

;; -- Simpler splitter container implementation

(defun splitter-container-2 (child1 splitter child2 &key (splitter-position 0.5))
  (make-instance 'splitter-container-2
                 :child1 child1
                 :splitter splitter
                 :child2 child2
                 :splitter-position splitter-position))                 

(defmethod measure ((container splitter-container-2) available-width available-height)
  (with-slots (child1 splitter child2 splitter-position) container
    (let ((splitter-left (or (layout-left splitter)
                             (round (* splitter-position available-width)))))
      (measure child1 
               splitter-left
               available-height)
      (measure splitter 4 available-height)
      (measure child2 (- available-width (+ splitter-left 4))
               available-height)
      ;; Return the container measures
      (values available-width available-height))))

(defmethod layout ((object splitter-container-2) left top width height)
  (ecase (scenic:orientation object)
      (:vertical
       (let ((running-top top))
         (dolist (widget (children object))
           (layout widget left running-top width (measured-height widget))
           (incf running-top (measured-height widget)))))
      (:horizontal
       (let ((running-left left))
         (dolist (widget (children object))
           (layout widget running-left top (measured-width widget) height)
           (incf running-left (measured-width widget))))))
    (call-next-method object left top width height))

(defmethod initialize-instance :after ((instance splitter-container-2) &rest initargs)
  (declare (ignore initargs))
  (mapc (lambda (widget)
          (setf (scenic:parent widget) instance))
        (children instance)))

(defmethod scenic::paint-order-walk ((object splitter-container-2) callback &key (after-callback nil))
  (when (funcall callback object)
    (mapc (lambda (child) (scenic::paint-order-walk child
                                            callback
                                            :after-callback after-callback))
          (children object)))
  (when after-callback
    (funcall after-callback object)))

(defun splitter-ex-3 ()
  (run-component
   (sizer (splitter-container-2
           (scenic::expander
            (stack 
             (background cl-colors:+white+
                         (filler))
             (flow-col (:spacing 2)
               (button (uniform-padding 5 (label "Foo")))
               (label "Bar")
               (label "Baz"))))
           (splitter)
           (scenic::expander
            (stack 
             (background cl-colors:+white+
                         (filler))
             (flow-col (:spacing 2)
               (label "Foo")
               (button (uniform-padding 5 (label "Bar")))
               (label "Baz")))))
          :width 500
          :height 500)))

;; (splitter-ex-3)
