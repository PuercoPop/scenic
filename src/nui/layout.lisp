;;-- * NUI layouts

(in-package :scenic.nui)

;;-- ** Flow row

(defmacro flow-row ((&key (spacing 0)) &body children)
  `(simple-horizontal-pack ,spacing (list ,@children)))

;;-- ** Flow col

(defmacro flow-col ((&key (spacing 0)) &body children)
  `(simple-vertical-pack ,spacing (list ,@children)))

;;-- ** Row
(defmacro row ((&key (layout :auto) (spacing 0)) &body body)
  (let ((layout-options (mapcar #'first body))
        (children (mapcar #'second body)))
    (if (zerop spacing)
        `(grid ',layout-options ',(list layout)
               (list (list :row ,@(mapcar (lambda (child) `(list :cell ,child))
                                          children))))
        `(grid ',(scenic::intersperse layout-options (list spacing :px))
               ',(list layout)
               (list (list :row ,@(mapcar (lambda (child) `(list :cell ,child))
                                          (scenic::intersperse children
                                                               `(placeholder ,spacing 0)))))))))

;;-- ** Col

(defmacro col ((&key (layout :auto) (spacing 0)) &body body)
  (let ((layout-options (mapcar #'first body))
        (children (mapcar #'second body)))
    (if (zerop spacing)
        `(grid ',(list layout) ',layout-options
               (list (list :column ,@(mapcar (lambda (child) `(list :cell ,child))
                                             children))))
        `(grid ',(list layout)
               ',(scenic::intersperse layout-options (list spacing :px))
               (list (list :column ,@(mapcar (lambda (child) `(list :cell ,child))
                                             (scenic::intersperse children
                                                                  `(placeholder 0 ,spacing)))))))))

;;-- ** Attached layouts

(defclass flow-layout (scenic::orientable)
  ((space-between-cells :accessor space-between-cells :initarg :space-between-cells :initform 0)))

(defmethod layout-measure ((layout flow-layout) object available-width available-height)
  (ecase (orientation layout)
    (:vertical
     (let* ((child-sizes (mapcar #'(lambda (widget)
                                     (multiple-value-list
                                      (scenic:measure widget available-width available-height)))
                                 (children object)))
            (vertical-size (+ (* (1- (length child-sizes))
                                 (space-between-cells layout))
                              (reduce #'+ (mapcar #'second child-sizes))))
            (horizontal-size (apply #'max (mapcar #'first child-sizes))))
       (values horizontal-size vertical-size)))
    (:horizontal
     (let* ((child-sizes (mapcar #'(lambda (widget)
                                     (multiple-value-list
                                      (scenic:measure widget available-width available-height)))
                                 (children object)))
            (horizontal-size (+ (* (1- (length child-sizes))
                                   (space-between-cells layout))
                                (reduce #'+ (mapcar #'first child-sizes))))
            (vertical-size (apply #'max (mapcar #'second child-sizes))))
       (values horizontal-size vertical-size)))))

(defmethod layout-layout ((layout flow-layout) object left top width height)
  (ecase (orientation layout)
    (:vertical
     (let ((running-top top))
       (dolist (widget (children object))
         (layout widget left running-top width (measured-height widget))
         (incf running-top (+ (measured-height widget) (space-between-cells layout))))))
    (:horizontal
     (let ((running-left left))
       (dolist (widget (children object))
         (layout widget running-left top (measured-width widget) height)
         (incf running-left (+ (measured-width widget) (space-between-cells layout))))))))

(defun flow-layout (&rest args)
  (apply #'make-instance 'flow-layout args))


(export '(row col flow-row flow-col flow-layout))

;;-- * Examples

#+scenic-test (progn
(run-component (row ()
                 ((1 :ext) (background (scenic::color cl-colors:+red+) (filler)))
                 ((2 :ext) (background (scenic::color cl-colors:+blue+) (filler)))) :title "Row layout")

(run-component
 (row (:spacing 5)
   ((1 :ext) (background (scenic::color cl-colors:+red+) (filler)))
   ((2 :ext) (background (scenic::color cl-colors:+blue+) (filler))))
 :title "Layout")


(run-component
 (col ()
   ((1 :ext) (background (scenic::color cl-colors:+red+) (filler)))
   ((1 :ext) (background (scenic::color cl-colors:+blue+) (filler)))))

(let ((component
       (col (:spacing 5)
         ((1 :ext) (background (scenic::color cl-colors:+red+) (filler)))
         ((1 :ext) (background (scenic::color cl-colors:+blue+) (filler))))))
  (run-component component))

(let ((component
       (col (:spacing 5)
         ((100 :px) (background (scenic::color cl-colors:+red+) (filler)))
         ((1 :ext) (background (scenic::color cl-colors:+blue+) (filler))))))
  (run-component component))
)
