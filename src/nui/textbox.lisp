;;-- * Textboxes

(in-package :scenic.nui)

;;-- ** Definition

(defclass nui-textbox (;nui-widget
                                        ;scenic::textbox
                       scenic::container1
                       scenic::padding-props
                       scenic::border-props
                       scenic::background-props)
  ((text :initarg :text :initform "" :accessor text)
   (width :initarg :width :initform nil :accessor width)
   (height :initarg :height :initform '(30 :px) :accessor height)))

;;-- ** Initialization

(defmethod initialize-instance :after ((textbox nui-textbox) &rest initargs)
  (setf (scenic:child textbox)
        (stack
         (background cl-colors:+lightgray+
                     (filler))
         (grid (list (or (width textbox) :auto))
               (list (height textbox))
               `((:row (:cell ,(background cl-colors:+lightgray+
                                           (uniform-padding
                                            3
                                            (border
                                             (list 0.0 0.0 0.0) 1
                                             (uniform-padding
                                              3
                                              (setf textbox
                                                    (textbox (text textbox) 0)))
                                             :corner-radius 2))))))))))

;;-- ** Rendering
(defmethod renderer-paint ((renderer scenic.renderer:gtk-renderer) (textbox nui-textbox)))

(defmethod scenic::paint ((textbox nui-textbox))
  (call-next-method))

;;-- ** API

(defun nui-textbox (&rest args)
  (apply #'make-instance 'nui-textbox args))


;;-- * Examples

(defun textbox-ex-1 ()
  (run-component
   (horizontal-pack* 0
                     '((1 :ext) (1 :ext))
                     (nui-textbox)
                     (nui-textbox ))
   :title "Textbox"))
