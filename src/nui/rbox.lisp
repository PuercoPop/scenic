(in-package :scenic.nui)

;;-- * R(ich) Boxes
;;-- R(rich) Boxes are UI boxes that can contain other widgets and also have
;;-- properties like padding, border, fill-color, etc. Very much like an HTML div element

(defclass abstract-rbox ()
  ((padding :initarg :padding
            :accessor rbox-padding
            :initform (scenic.style:padding 0))
   (border :initarg :border
           :accessor rbox-border
           :initform nil)
   (fill-color :accessor fill-color :initarg :fill-color :initform nil)
   (corner-radius :accessor corner-radius
                  :initarg :corner-radius
                  :initform nil))
  (:documentation "A container that supports multiple properties. Similar to an HTML element"))

(defmethod initialize-instance :after
    ((rbox abstract-rbox)
     &key padding-top padding-right padding-bottom padding-left
       border-top border-right border-bottom border-left)
  (setf (rbox-border rbox)
        (scenic.style::merge-style-spec (rbox-border rbox)
                                        border-top border-right
                                        border-bottom border-left))
  (setf (rbox-padding rbox)
        (scenic.style::merge-padding-spec
         (rbox-padding rbox)
         padding-top padding-right
         padding-bottom padding-left)))                                          

(defmethod padding-left ((rc abstract-rbox))
  (scenic.style:padding-left (rbox-padding rc)))

(defmethod padding-top ((rc abstract-rbox))
  (scenic.style:padding-top (rbox-padding rc)))

(defmethod padding-right ((rc abstract-rbox))
  (scenic.style:padding-right (rbox-padding rc)))

(defmethod padding-bottom ((rc abstract-rbox))
  (scenic.style:padding-bottom (rbox-padding rc)))

(defmethod border-top ((rc abstract-rbox))
  (first (scenic.style:expand-style-spec (rbox-border rc))))

(defmethod border-right ((rc abstract-rbox))
  (second (scenic.style:expand-style-spec (rbox-border rc))))

(defmethod border-bottom ((rc abstract-rbox))
  (third (scenic.style:expand-style-spec (rbox-border rc))))

(defmethod border-left ((rc abstract-rbox))
  (nth 3 (scenic.style:expand-style-spec (rbox-border rc))))

(defmethod total-border-width ((rc abstract-rbox))
  (+ (or (anaphora:aand (border-left rc)
                        (scenic.style:border-width anaphora:it))
         0)
     (or (anaphora:aand (border-right rc)
                        (scenic.style:border-width anaphora:it))
         0)))

(defmethod total-border-height ((rc abstract-rbox))
  (+ (or (anaphora:aand (border-top rc)
                        (scenic.style:border-width anaphora:it))
         0)
     (or (anaphora:aand (border-bottom rc)
                        (scenic.style:border-width anaphora:it))
         0)))

(defclass rbox-1 (abstract-rbox scenic::container1)
  ())

(defun measure-padding (object available-width available-height)
  (multiple-value-bind (child-width child-height)
      (measure (child object)
               (- available-width
                  (padding-left object) (padding-right object))
               (- available-height
                  (padding-top object) (padding-bottom object)))
    (let ((width (+ (padding-left object) (padding-right object) child-width))
          (height (+ (padding-top object) (padding-bottom object) child-height)))
      (set-measured object width height))))

(defun layout-padding (object left top width height)
  (layout (child object)
          (+ left (padding-left object))
          (+ top (padding-top object))
          (- width (+ (padding-left object) (padding-right object)))
          (- height (+ (padding-top object) (padding-bottom object))))
  (set-layout object left top width height))

(defun measure-border (object dimensions)
  (let ((width (+ (* 2 (border-width object)) (first dimensions)))
        (height (+ (* 2 (border-width object)) (second dimensions))))
    (set-measured object width height)))

(defun layout-border (object left top width height)
  (layout (child object)
          (+ left (border-width object))
          (+ top (border-width object))
          (- width (* 2 (border-width object)))
          (- height (* 2 (border-width object))))
  (set-layout object left top width height))

(defmethod measure ((object rbox-1) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object) available-width available-height)
    (let ((measured-width (+ width
                             (total-border-width object)
                             (padding-left object)
                             (padding-right object)))
          (measured-height (+ height
                              (total-border-height object)
                              (padding-top object)
                              (padding-bottom object))))
      (set-measured object measured-width measured-height))))

(defmethod layout ((object rbox-1) left top width height)
  (let ((left-layout (+ left
                        ;;(or (anaphora:aand (border-left object)
                        ;;         (scenic.style:border-width anaphora:it))
                        ;;    0)
                        (padding-left object)))
        (width-layout (- width
                         ;;(or
                         ;; (and (rbox-border object)
                         ;;      (border-width object))
                         ;; 0)
                         (padding-right object)))
        (top-layout (+ top
                       ;;(or
                       ;; (and (rbox-border object)
                       ;;      (border-width object))
                       ;; 0)
                       (padding-top object)))
        (height-layout (- height
                          ;;(or (and (rbox-border object)
                          ;;         (border-width object))
                          ;;    0)
                          (padding-bottom object))))
    (layout (child object)
            left-layout
            top-layout
            width-layout
            height-layout)
    (set-layout object left top width height)))

(defmethod scenic::paint ((object rbox-1))
  (scenic-utils::draw-box (layout-left object)
                          (layout-top object)
                          (layout-width object)
                          (layout-height object)
                          :border (rbox-border object)
                          :fill-color (fill-color object)
                          :corner-radius (corner-radius object))
  (call-next-method))

(defun rbox-1 (child &rest args)
  (apply #'make-instance 'rbox-1 :child child args))

;; Should we do top widget a rbox??
(defclass rbox (abstract-rbox scenic::container)
  ((layout :initarg :layout
           :initform (make-instance 'flow-layout :orientation :horizontal)
           :accessor layout-of))
  (:documentation "Generic R(ich)Box with attached layout"))

(defmethod scenic:measure ((object rbox) available-width available-height)
  (multiple-value-bind (width height)
      (layout-measure (layout-of object) object available-width available-height)
    (let ((measured-width (+ width
                             (total-border-width object)
                             (padding-left object)
                             (padding-right object)))
          (measured-height (+ height
                              (total-border-height object)
                              (padding-top object)
                              (padding-bottom object))))
      (set-measured object measured-width measured-height))))

(defmethod scenic:layout ((object rbox) left top width height)
  (let ((layout-left (+ left
                        ;;(border-width object)
                        (padding-left object)))
        (layout-width (- width
                         ;;(border-width object)
                         (padding-right object)))
        (layout-top (+ top
                       ;;(border-width object)
                       (padding-top object)))
        (layout-height (- height
                          ;;(border-width object)
                          (padding-bottom object))))
    (layout-layout (layout-of object) object
                   layout-left layout-top
                   layout-width layout-height)
    (set-layout object left top width height)))

(defmethod scenic::paint ((object rbox))
  (scenic-utils::draw-box (layout-left object)
                           (layout-top object)
                           (layout-width object)
                           (layout-height object)
                           :border (rbox-border object)
                           :fill-color (fill-color object)
                           :corner-radius (corner-radius object))
  (call-next-method))

(defmacro rbox (options &body children)
  `(make-instance 'rbox
                  :children (list ,@children)
                  ,@options))

(export '(rbox rbox-1))


(defclass rbox-2 (scenic::container1
                            padding-props
                            border-props
                            background-props)
  ())



;; * Examples

(defun rbox-ex-1 ()
  (rbox-1 (label "Hello" :size 16 :color '(1 1 1))
                                :fill-color cl-colors:+red+))

(defun rbox-ex-2 ()
  (rbox-1 (label "Hello" :size 16 :color '(1 1 1))
                                :fill-color cl-colors:+red+
                                :border (scenic.style:border 5 :solid cl-colors:+white+)
                                :padding (scenic.style:padding 30)))

(defun rbox-ex-3 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                    :space-between-cells 10)
                                       :fill-color cl-colors:+red+
                                       :border (scenic.style:border 6 :solid cl-colors:+white+)
                                       :padding (scenic.style:padding 20)

                                       )
                (label "Hello" :weight :bold :color cl-colors:+white+)
                (label "World")))

(defun rbox-ex-4 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                    :space-between-cells 10)
                                       :fill-color cl-colors:+brown+
                                       :border (list
                                                (scenic.style:border 6 :solid cl-colors:+white+)
                                                (scenic.style:border 6 :solid cl-colors:+red+)
                                                (scenic.style:border 6 :solid cl-colors:+green+)
                                                (scenic.style:border 6 :solid cl-colors:+blue+))
                                       :padding (scenic.style:padding 20)

                                       )
                (label "Hello" :weight :bold :color cl-colors:+white+)
                (label "World")))

(defun rbox-ex-5 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                    :space-between-cells 10)
                                       :fill-color cl-colors:+brown+
                                       :border (list
                                                (scenic.style:border 6 :solid cl-colors:+white+)
                                                (scenic.style:border 10 :solid cl-colors:+red+)
                                                (scenic.style:border 14 :solid cl-colors:+green+)
                                                (scenic.style:border 18 :solid cl-colors:+blue+))
                                       :padding (scenic.style:padding 20)

                                       )
                (label "Hello" :weight :bold :color cl-colors:+white+)
                (label "World")))

(defun rbox-ex-6 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                    :space-between-cells 10)
                                       :fill-color cl-colors:+white+
                                       :border (list
                                                (scenic.style:border 6 :solid cl-colors:+brown+)
                                                (scenic.style:border 6 :solid cl-colors:+red+)
                                                (scenic.style:border 6 :solid cl-colors:+green+)
                                                (scenic.style:border 6 :solid cl-colors:+blue+))
                                       :padding (scenic.style:padding 20)
                                       :corner-radius (list nil 20 nil 20)

                                       )
                (label "Hello" :weight :bold :color cl-colors:+white+)
                (label "World")))

(defun rbox-ex-7 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                    :space-between-cells 10)
                                       :fill-color cl-colors:+white+
                                       :border (list
                                                (scenic.style:border 6 :solid cl-colors:+brown+)
                                                (scenic.style:border 6 :solid cl-colors:+red+)
                                                (scenic.style:border 6 :solid cl-colors:+green+)
                                                (scenic.style:border 6 :solid cl-colors:+blue+))
                                       :padding (scenic.style:padding 20)
                                       :corner-radius (list 20 nil 20 nil)

                                       )
                (label "Hello" :weight :bold :color cl-colors:+white+)
                (label "World")))

(defun rbox-ex-8 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border nil
                                                        :padding (scenic.style:padding 50 10 0 0)
                                                        :corner-radius (list 10 20 30 40)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold :color cl-colors:+white+)
                                 (label "World")))

(defun rbox-ex-9 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border nil
                                                        :padding (scenic.style:padding 50 10 0 0)
                                                        :corner-radius (list 10 nil 30 nil)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold :color cl-colors:+white+)
                                 (label "World")))

(defun rbox-ex-10 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (scenic.style:border 6 :solid cl-colors:+red+)
                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius (list 10 nil 30 nil)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

(defun rbox-ex-11 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (scenic.style:border 4 :solid cl-colors:+red+)
                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius (list nil 10 nil 30)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

(defun rbox-ex-12 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (list (scenic.style:border 6 :solid cl-colors:+red+)
                                                                      (scenic.style:border 6 :solid cl-colors:+red+)
                                                                      nil
                                                                      (scenic.style:border 6 :solid cl-colors:+red+))

                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius (list 20 20 nil nil)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

(defun rbox-ex-13 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (list nil
                                                                      (scenic.style:border 6 :solid cl-colors:+red+)
                                                                      (scenic.style:border 6 :solid cl-colors:+red+)
                                                                      (scenic.style:border 6 :solid cl-colors:+red+))

                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius nil
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

(defun rbox-ex-14 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (list (scenic.style:border 5 :solid cl-colors:+red+)
                                                                      (scenic.style:border 10 :solid cl-colors:+green+)
                                                                      (scenic.style:border 15 :solid cl-colors:+yellow+)
                                                                      (scenic.style:border 20 :solid cl-colors:+blue+))

                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius (list 20 20 20 20)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

(defun rbox-ex-15 ()
  (rbox (:layout (flow-layout :orientation :horizontal
                                                                     :space-between-cells 10)
                                                        :border (list (scenic.style:border 5 :solid cl-colors:+red+)
                                                                      (scenic.style:border 5 :solid cl-colors:+green+)
                                                                      (scenic.style:border 5 :solid cl-colors:+yellow+)
                                                                      (scenic.style:border 5 :solid cl-colors:+blue+))

                                                        :padding (scenic.style:padding 50)
                                                        :corner-radius (list 20 20 20 20)
                                                        :fill-color cl-colors:+white+

                                                        )
                                 (label "Hello" :weight :bold)
                                 (label "World")))

;; (run-component (rbox-ex-1) :title "Rich box")
;; (run-component (rbox-ex-2) :title "Rich box")
;; (run-component (rbox-ex-3) :title "Rich box")

(defun rbox-ex-grid ()
  (flet ((cell (comp)
           (uniform-padding 5 comp)))
    (let ((hsize (/ 800 5)))
      (let ((grid (grid nil nil
                        (list (list :row
                                    (list :cell (cell (rbox-ex-1)))
                                    (list :cell (cell (rbox-ex-2)))
                                    (list :cell (cell (rbox-ex-3))))
                              (list :row
                                    (list :cell (cell (rbox-ex-4)))
                                    (list :cell (cell (rbox-ex-5)))
                                    (list :cell (cell (rbox-ex-6))))
                              (list :row
                                    (list :cell (cell (rbox-ex-7)))
                                    (list :cell (cell (rbox-ex-8)))
                                    (list :cell (cell (rbox-ex-9))))
                              (list :row
                                    (list :cell (cell (rbox-ex-10)))
                                    (list :cell (cell (rbox-ex-11)))
                                    (list :cell (cell (rbox-ex-12))))
                              (list :row
                                    (list :cell (cell (rbox-ex-13)))
                                    (list :cell (cell (rbox-ex-14)))
                                    (list :cell (cell (rbox-ex-15))))))))
        (run-component grid
                       :width 500
                       :height 800
                       :title "Rich box")))))

(defun rbox-ex-grid-1 ()
  (flet ((cell (comp)
           (sizer comp :width 120 :height 120)))
    (let ((grid (flow-col (:spacing 5)
                  (flow-row (:spacing 5)
                    (cell (rbox-ex-1))
                    (cell (rbox-ex-2))
                    (cell (rbox-ex-3)))
                  (flow-row (:spacing 5)
                    (cell (rbox-ex-4))
                    (cell (rbox-ex-5))
                    (cell (rbox-ex-6)))
                  (flow-row (:spacing 5)
                    (cell (rbox-ex-7))
                    (cell (rbox-ex-8))
                    (cell (rbox-ex-9)))
                  (flow-row (:spacing 5)
                    (cell (rbox-ex-10))
                    (cell (rbox-ex-11))
                    (cell (rbox-ex-12)))
                  (flow-row (:spacing 5)
                    (cell (rbox-ex-13))
                    (cell (rbox-ex-14))
                    (cell (rbox-ex-15))))))
      (run-component grid
                     :width 500
                     :height 700
                     :title "Rich box"))))

;; (rbox-ex-grid)
;; (rbox-ex-grid-1)
