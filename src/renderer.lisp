(in-package :scenic.renderer)

;;-- * Renderers

;;-- ** Overview
;;-- Renderers are in charge of drawing widgets. They contain default options.

;;-- The current renderer is bound to this variable:
(defvar *renderer* nil)

(defvar *renderers* (make-hash-table))

(defun find-renderer (name &optional (error-p t))
  (or (gethash name *renderers*)
      (and error-p
           (error "Renderer not defined: ~A" name))))

;;-- ** Using
;;-- Use a specific renderer via the macro *with-renderer*:

(defmacro with-renderer (name &body body)
  `(let ((*renderer* (find-renderer ',name)))
     ,@body))

(defun set-renderer (name-or-renderer)
  (check-type name-or-renderer (or symbol renderer))
  "Set the renderer globally"
  (setf *renderer* (if (symbolp name-or-renderer)
                       (find-renderer name-or-renderer)
                       name-or-renderer)))

;;-- ** Definition

;;-- The top-level renderer class. Nothing interesting here:

(defclass renderer ()
  ())


;;-- The macro for defining renderers *defrenderer*
;;-- A renderer has a set of properties, and they are defined in categories
;;-- For example:
;;--
;;-- #+BEGIN_EXAMPLE
;;-- (defrenderer default-renderer (renderer)
;;--  ((panel (color (color "#d6d6d6")))
;;--   (button (padding '(5 10)
;;--                    color (color "#f3f3f3")
;;--                    color[pressed] (color "#b8b8b8")
;;--                    color[hover] (color "#b1b1b1")
;;--                    text (list "Helvetica" :regular 13 cl-colors:+black+)
;;--                    text[hover] (list "Helvetica" :regular 13 cl-colors:+black+)
;;--                    border[hover] (list 1 :solid (color "#b1b1b1"))))
;;-- #+END_EXAMPLE
;;--
;;-- In the above example, "panel" and "button" are the top categories, and they are used
;;-- to flatten the final properties accessors, like: panel-color, button-padding, button-color, etc.
;;-- The idea of this is twofold:
;;-- First, to provide a convenient syntax for defining properties (not having to repeat the category, say, "button", everywhere)
;;-- And second, take advantange of CLOS inheritance, as renderer properties are CLOS class accessors that can be overwritten down in the renderers hierarchy.

(defmacro defrenderer (name super categories &rest options)
  (flet ((expand-prop-name (category-chain prop)
           (intern
            (format nil "~{~a~^-~}" (reverse (cons prop category-chain))))))
    (labels ((collect-properties (category-chain category)
               (destructuring-bind (category props &rest sub-categories) category
                 (append
                  (loop for prop in props by #'cddr
                       for value in (rest props) by #'cddr
                     collect
                       (let ((prop-name (expand-prop-name (cons category category-chain) prop)))
                         `(,prop-name :initarg ,(alexandria:make-keyword prop-name)
                                      :accessor ,prop-name
                                      :initform ,value)))
                  (apply #'append
                         (mapcar (lambda (sub-category)
                                   (collect-properties (cons category category-chain) sub-category))
                                 sub-categories))))))             
      `(progn
         (defclass ,name ,super
           ,(loop for category in categories
               appending (collect-properties nil category))
           ,@options)
         (setf (gethash ',name *renderers*) (make-instance ',name))))))

;;-- ** Default renderer

(defrenderer default-renderer (renderer)
  ((panel (color (color "#d6d6d6")))
   (button (padding '(5 10)
                    color (color "#f3f3f3")
                    color[pressed] (color "#b8b8b8")
                    color[hover] (color "#b1b1b1")
                    text (list "Helvetica" :regular 13 cl-colors:+black+)
                    text[hover] (list "Helvetica" :regular 13 cl-colors:+black+)
                    border[hover] (list 1 :solid (color "#b1b1b1"))))
   (list (background-color cl-colors:+white+
                           border nil
                           border[focus] (list 1 :solid cl-colors:+blue+))
         (li (background-color cl-colors:+white+
                                padding 5
                                background-color[selected] cl-colors:+lightblue+
                                text-color cl-colors:+black+
                                text-color[selected] cl-colors:+white+))))
  (:documentation "Default renderer"))

;;-- ** GTK renderer
;;-- This is a GTK-like renderer

(defrenderer gtk-renderer (renderer)
  ((button (padding (padding 5 10)
            corner-radius 3
            color (linear-gradient :bottom
                                   (color "#f3f3f3")
                                   (color "#dadada"))
            color[pushed] (color "#b8b8b8")
            color[hover] (color "#b1b1b1")
            text (text "Helvetica" :regular 13 cl-colors:+black+)
            text[pushed] (text "Helvetica" :regular 13 cl-colors:+black+)
            border (border 1 :solid (color "#b1b1b1"))
            border[pushed] (border 1 :solid (color "#b1b1b1"))
            border[hover] (border 1 :solid (color "#b1b1b1"))))
   (panel (color (color "#d6d6d6")))
   (tabview ()
            (tab (color (color "#d7d7d7")
                        color[active] (color "#ececec")))))
  (:documentation "GTK like renderer"))

;;-- ** Windows renderer

(defrenderer wnd7-renderer (renderer)
  ((button (padding (padding 5 10)
                    corner-radius 3
                    color (linear-gradient :bottom
                                           (color "#f3f3f3") (color "#dadada"))
                    color[pushed] (color "#b8b8b8")
                    color[hover] (color "#b1b1b1")
                    text[pushed] (text "Helvetica" :regular 13 cl-colors:+black+)
                    text[hover] (text "Helvetica" :regular 13 cl-colors:+black+)
                    border (border 1 :solid (color "#b1b1b1"))
                    border[pushed] (border 1 :solid (color "#b1b1b1"))
                    border[hover] (border 1 :solid (color "#b1b1b1")))
           (primary (border (border 1 :solid (color "#7fa2ba"))
                            color (color "#bde6fb"))))
   (panel (color (color "#d6d6d6"))))
  (:documentation "GTK like renderer"))

;;-- ** Material renderer

(defrenderer material-renderer (renderer)
  ((button (padding (padding 5 10)
                    corner-radius 3
                    color (list :linear-gradient :bottom
                                (color "#f3f3f3") (color "#dadada"))
                    color[pushed] (color "#b8b8b8")
                    color[hover] (color "#b1b1b1")
                    text[pushed] (text "Helvetica" :regular 13 cl-colors:+black+)
                    text[hover] (text "Helvetica" :regular 13 cl-colors:+black+)
                    border (border 1 :solid (color "#b1b1b1"))
                    border[pushed] (border 1 :solid (color "#b1b1b1"))
                    border[hover] (border 1 :solid (color "#b1b1b1")))
           (primary (border (list 1 :solid (color "#7fa2ba"))
                            color (color "#bde6fb"))))
   (panel (color cl-colors:+white+)))
  (:documentation "Material design renderer"))

;;-- ** Rect renderer
;;-- This is a very simple squared and flat renderer. The idea is to render widgets as simply and plain as possible (using just rectangles, nothing fancy)

(defrenderer rect-renderer (renderer)
  ((button (padding (padding 5 10)
                    color (color "#dadada")
                    color[pushed] (color "#b8b8b8")
                    color[hover] (color "#b1b1b1")
                    text[pushed] (text "Helvetica" :regular 13 cl-colors:+black+)
                    text[hover] (text "Helvetica" :regular 13 cl-colors:+black+)
                    border (border 1 :solid (color "#b1b1b1"))
                    border[pushed] (border 1 :solid (color "#b1b1b1"))
                    border[hover] (border 1 :solid (color "#b1b1b1"))
                    corner-radius nil)
           (primary (border (border 1 :solid (color "#7fa2ba"))
                            color (color "#bde6fb"))))
   (panel (color cl-colors:+lightgray+))
   (scrollbar (width 4))
   (slider (color (color "#dadada")
                  color[pushed] (color "#b8b8b8")
                  color[hover] (color "#b1b1b1")))
   (tabview ()
            (tab (color (color "#b8b8b8")
                        color[active] (color "#dadada")
                        color[hover] (color "#b1b1b1")))))
  (:documentation "Rect renderer"))

(setf *renderer* (make-instance 'default-renderer))
