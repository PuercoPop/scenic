(in-package :scenic)

;;; SLIDER classes.

(defclass slider (widget orientable)
  ((min-value :accessor min-value :initarg :min-value :initform nil)
   (max-value :accessor max-value :initarg :max-value :initform nil)
   (page-size :accessor page-size :initarg :page-size :initform nil)
   (scroll-delta :accessor scroll-delta :initarg :scroll-delta :initform 20)
   (current-min-position :initarg :current-min-position :initform nil)))

(defun get-slider-length-position (slider)
  (let* ((screen-dimension (ifhorizontal slider
                                         (layout-width slider)
                                         (layout-height slider)))
         (extent (- (max-value slider) (min-value slider)))
         (rel-page-size (/ (page-size slider) extent))
         (walker-width-or-height (min (max 10 (floor (* rel-page-size
                                                        screen-dimension)))
                                      (ifhorizontal slider
                                                    (layout-width slider)
                                                    (layout-height slider))))
         (rel-position (/ (- (current-min-position slider)
                             (min-value slider))
                          extent))
         (position (min (round (* rel-position screen-dimension))
                        (- screen-dimension walker-width-or-height))))
    (values walker-width-or-height position)))

(defun get-walker-coordinates (slider)
  (multiple-value-bind (walker-width-or-height position)
      (get-slider-length-position slider)
    (ifhorizontal slider
                  (values (+ (layout-left slider) position)
                          (layout-top slider)
                          walker-width-or-height
                          (layout-height slider))
                  (values (layout-left slider)
                          (+ (layout-top slider) position)
                          (layout-width slider)
                          walker-width-or-height))))

(defmethod paint ((object slider))
  (scenic.draw:rectangle (layout-left object) (layout-top object)
                       (layout-width object)
                       (layout-height object))
  (scenic.draw:set-source-rgb 0.6 0.6 0.6)
  (scenic.draw:fill-path)
  (multiple-value-bind (left top width height)
      (get-walker-coordinates object)
    (draw-button-raw left top width height nil)))

(defmethod initialize-instance :after ((instance slider) &rest initargs)
  (declare (ignore initargs))
  (let ((dragging nil)
        (rel-pos 0))
    (add-event-handler instance :mouse-button-down :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (multiple-value-bind (left top width height)
                               (get-walker-coordinates instance)
                             (when (in-rectangle (mouse-x event) (mouse-y event)
                                                 left top width height)
                               (setf rel-pos (ifhorizontal instance
                                                           (- (mouse-x event) left)
                                                           (- (mouse-y event) top)))
                               (setf dragging t)
                               (capture-mouse instance))))))
    (add-event-handler instance :mouse-button-up :bubble
                       (lambda (instance event)
                         (when (= 1 (mouse-button event))
                           (setf dragging nil)
                           (release-mouse instance))))
    (add-event-handler instance :mouse-move :bubble
                       (lambda (instance event)
                         (when dragging
                           (let* ((screen-dimension (ifhorizontal instance
                                                                  (layout-width instance)
                                                                  (layout-height instance)))
                                  (screen-position
                                   (ifhorizontal instance
                                                 (- (mouse-x event) (layout-left instance))
                                                 (- (mouse-y event) (layout-top instance))))
                                  (extent (1+ (- (max-value instance)
                                                 (min-value instance))))
                                  (multiplier (/ extent screen-dimension))
                                  (new-current-min-pos (- (* multiplier screen-position)
                                                          (* multiplier rel-pos))))
                             (setf (current-min-position instance) new-current-min-pos)))))))

(DEFMETHOD CURRENT-MIN-POSITION ((SCENIC-UTILS::OBJECT SLIDER))
   (WITH-SLOTS (CURRENT-MIN-POSITION)
       SCENIC-UTILS::OBJECT
     (IF (SCENIC-UTILS::VALUE-MODEL-P CURRENT-MIN-POSITION)
         (SCENIC-UTILS::VALUE CURRENT-MIN-POSITION)
         CURRENT-MIN-POSITION)))

(defmethod (setf current-min-position) (new-value (slider slider))
  (when (> new-value (- (max-value slider) (page-size slider)))
    (setf new-value (- (max-value slider) (page-size slider))))
  (when (< new-value (min-value slider))
    (setf new-value (min-value slider)))
  (when (not (= new-value (current-min-position slider)))
    (with-slots (current-min-position) slider
      (if (scenic-utils::value-model-p current-min-position)
          (setf (scenic-utils::value current-min-position) new-value)
          (setf current-min-position new-value))
      (invalidate slider)
      (on-event slider
                :position-changed
                (make-instance 'event)
                nil))))

;;; ARROW class.

(defclass arrow (widget)
  ((direction :accessor direction :initarg :direction :initform nil)
   (size :initarg :size
         :accessor arrow-size
         :initform 15)))

(defmethod measure ((object arrow) available-width available-height)
  (set-measured object (arrow-size object) (arrow-size object)))

(defmethod layout ((object arrow) left top width height)
  (set-layout object left top (arrow-size object) (arrow-size object)))

(defmethod paint ((object arrow))
  (ecase (direction object)
    (:left
     (scenic.draw:move-to (+ (layout-left object) 5) (+ (layout-top object) 8))
     (scenic.draw:line-to (+ (layout-left object) 11) (+ (layout-top object) 2))
     (scenic.draw:line-to (+ (layout-left object) 11) (+ (layout-top object) 13))
     (scenic.draw:set-source-rgb 0 0 0)
     (scenic.draw:fill-path))
    (:right
     (scenic.draw:move-to (+ (layout-left object) 11) (+ (layout-top object) 8))
     (scenic.draw:line-to (+ (layout-left object) 5) (+ (layout-top object) 2))
     (scenic.draw:line-to (+ (layout-left object) 5) (+ (layout-top object) 13))
     (scenic.draw:set-source-rgb 0 0 0)
     (scenic.draw:fill-path))
    (:up
     (scenic.draw:move-to (+ (layout-left object) 9) (+ (layout-top object) 5))
     (scenic.draw:line-to (+ (layout-left object) 3) (+ (layout-top object) 11))
     (scenic.draw:line-to (+ (layout-left object) 14) (+ (layout-top object) 11))
     (scenic.draw:set-source-rgb 0 0 0)
     (scenic.draw:fill-path))
    (:down
     (scenic.draw:move-to (+ (layout-left object) 9) (+ (layout-top object) 11))
     (scenic.draw:line-to (+ (layout-left object) 3) (+ (layout-top object) 5))
     (scenic.draw:line-to (+ (layout-left object) 14) (+ (layout-top object) 5))
     (scenic.draw:set-source-rgb 0 0 0)
     (scenic.draw:fill-path))))

;;; SCROLLBAR class.

(defclass scrollbar (container1 orientable)
  ((min-value :accessor min-value :initarg :min-value :initform nil)
   (max-value :accessor max-value :initarg :max-value :initform nil)
   (page-size :accessor page-size :initarg :page-size :initform nil)
   (scroll-delta :accessor scroll-delta :initarg :scroll-delta :initform 20)
   (current-min-position :accessor current-min-position
                         :initarg :current-min-position :initform nil)
   (slider :accessor slider :initarg :slider :initform nil)
   (width :initarg :width
          :accessor scrollbar-width
          :initform 15)))

(defmethod initialize-instance :after ((instance scrollbar) &rest initargs)
  (declare (ignore initargs))
  (let* ((slider (make-instance
                  'slider
                  :orientation (orientation instance)
                  :min-value (min-value instance)
                  :max-value (max-value instance)
                  :page-size (page-size instance)
                  :current-min-position
                  (current-min-position instance)))
         (btn-left (button (aligner
                            (sizer (fa-icon (ifhorizontal instance
                                                          "caret_left"
                                                          "caret_up"))
                                   :height (- (scrollbar-width instance) 4)
                                   :width (- (scrollbar-width instance) 4)))))
         (btn-right (button (aligner
                             (sizer (fa-icon (ifhorizontal instance
                                                           "caret_right"
                                                           "caret_down"))
                                    :height (- (scrollbar-width instance) 4)
                                    :width (- (scrollbar-width instance) 4)))))
         (grid (ifhorizontal instance
                             (make-instance 'scenic-grid:grid
                                            :column-layout-options '(:auto (1 :ext) :auto)
                                            :row-layout-options `((,(scrollbar-width instance) :px))
                                            :children-descriptions `((:row (:cell ,btn-left)
                                                                           (:cell ,slider)
                                                                           (:cell ,btn-right))))
                             (make-instance
                              'scenic-grid:grid
                              :column-layout-options `((,(scrollbar-width instance) :px))
                              :row-layout-options '(:auto (1 :ext) :auto)
                              :children-descriptions `((:column (:cell ,btn-left)
                                                                (:cell ,slider)
                                                                (:cell ,btn-right)))))))
    (setf (child instance) grid)
    (setf (slider instance) slider)
    (add-event-handler slider :position-changed :bubble
                       (lambda (object event)
                                        ; Using slot-value to prevent
                                        ; passing down the value to
                                        ; the slider via setf (thus
                                        ; entering an infinite loop).
                         (setf (slot-value instance 'current-min-position)
                               (current-min-position object))
                         (on-event instance :position-changed event nil)))
    (add-event-handler btn-left :click :bubble
                       (lambda (object event)
                         (declare (ignore object event))
                         (setf (current-min-position instance)
                               (- (current-min-position slider)
                                  (scroll-delta slider)))))
    (add-event-handler btn-right :click :bubble
                       (lambda (object event)
                         (declare (ignore object event))
                         (setf (current-min-position instance)
                               (+ (current-min-position slider)
                                  (scroll-delta instance)))))
    (add-event-handler instance :mouse-button-down :bubble #'handle-scroll-wheel)))

(defmethod scroll-up ((scrollbar scrollbar) &optional (delta (scroll-delta scrollbar)))
  (setf (current-min-position scrollbar)
        (max 0 (- (current-min-position scrollbar) delta))))

(defmethod scroll-down ((scrollbar scrollbar) &optional (delta (scroll-delta scrollbar)))
  (incf (current-min-position scrollbar) delta))

(defmethod handle-scroll-wheel ((scrollbar scrollbar) event)
  (when (= (mouse-button event) 4)
    (scroll-up scrollbar))
  (when (= (mouse-button event) 5)
    (scroll-down scrollbar)))

(pass-to-child scrollbar slider min-value)
(pass-to-child scrollbar slider max-value)
(pass-to-child scrollbar slider page-size)
(pass-to-child scrollbar slider current-min-position)
(pass-to-child scrollbar slider scroll-delta)
