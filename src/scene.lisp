;;-- * Scenes

;;-- A Scene is the top-level component to which the main widget is attached. It is at the top of the widget hierarchy and layout, invalidation, events, etc, are handled through it.
;;-- A Scene belongs to a backend window.

(in-package :scenic)

;;-- ** Scene inspector

(defclass scene-inspector ()
  ((pointed-widget :accessor pointed-widget :initform nil)
   (selected-widget :accessor selected-widget :initform nil)))

(defun inspect-widget (inspector widget scene)
  (when (pointed-widget inspector)
    (invalidate (pointed-widget inspector)))
  (setf (pointed-widget inspector) widget)
  (format t "Inspecting: ~A~%" widget)
  (swank:inspect-in-emacs widget))

(defun highlight-inspected-widget (inspector)
  (let ((widget (pointed-widget inspector)))
    (when widget
      (scenic.draw:rectangle (layout-left widget)
                             (layout-top widget)
                             (layout-width widget)
                             (layout-height widget))
      (scenic.draw:set-source-rgba 0 0 1 0.5)
      (scenic.draw:fill-path)
      (scenic.draw:move-to (layout-left widget)
                           (+ (layout-top widget) 8))
      (scenic.draw:set-source-color cl-colors:+black+)
      (scenic.draw:set-font-size 8)
      (scenic.draw:show-text (princ-to-string widget))
      )))

;;-- ** SCENE class.

(declaim (optimize (debug 3)))

(defclass scene ()
  ((widget :accessor widget :initarg :widget :initform nil)
   (width :accessor width :initarg :width :initform 1024)
   (height :accessor height :initarg :height :initform 768)
   (last-widget-chain :accessor last-widget-chain :initarg :last-widget-chain :initform nil)
   (mouse-captors :accessor mouse-captors :initarg :mouse-captors :initform nil)
   (dirty :accessor dirty :initarg :dirty :initform t)
   (dirty-list :accessor dirty-list :initarg :dirty-list :initform nil)
   (layedout :accessor layedout :initarg :layedout :initform nil)
   (rectangle-to-redraw :accessor rectangle-to-redraw :initarg :rectangle-to-redraw :initform nil)
   (sdl-surface :accessor sdl-surface :initarg :sdl-surface :initform nil)
   (sdl-window :accessor sdl-window :initarg :sdl-window :initform nil)
   (step-functions :accessor step-functions :initform nil)
   (renderer :accessor renderer :initarg :renderer :initform scenic.renderer:*renderer* :type (or null scenic.renderer:renderer))
   (inspector :accessor inspector :initform (make-instance 'scene-inspector))
   (stylesheet :accessor stylesheet :initarg :stylesheet :initform nil :type (or null scenic.style:stylesheet))
   (focusables :accessor focusables :initarg :focusables :initform nil)
   (focused-index :accessor focused-index :initarg :focused-index :initform nil)
   (on-scene-init :accessor on-scene-init :initarg :on-scene-init :initform nil)))

;;-- ** Scene invalidation
(defun invalidate-scene (scene)
  (setf (dirty scene) t)
  (setf (dirty-list scene) nil))

(defun get-scene (widget)
  (if (typep widget 'scene)
      widget
      (if (and widget (parent widget))
          (get-scene (parent widget)))))

(defun capture-mouse (widget)
  (let ((scene (get-scene widget)))
    (if (not (member widget (mouse-captors scene)))
        (push widget (mouse-captors scene)))))

(defun release-mouse (widget)
  (let ((scene (get-scene widget)))
    (setf (mouse-captors scene)
          (remove widget (mouse-captors scene)))))

(defun invalidate (widget)
  (bwhen (scene (get-scene widget))
    (setf (dirty scene) t)
    (push widget (dirty-list scene))))

;;-- ** Layout

(defun mark-for-layout (widget)
  (bwhen (scene (get-scene widget))
    (setf (layedout scene) nil)))

;;-- ** Painting

(defun widget-paint-member (object list)
  (cond ((null list)
         nil)
        ((let ((head (first list)))
           (or (eq object head)
               (and (paint-order-number object)
                    (> (paint-order-number object)
                       (paint-order-number head))
                    ;; The affected-rect of a widget is its visible area, taking into account clips and scrolls.
                    (and (affected-rect object)
                         (affected-rect head)
                         (rect-intersect (affected-rect object)
                                         (affected-rect head))))))
         t)
        (t (widget-paint-member object (rest list)))))

(defun visible-bounding-box (widget)
  "The visible area of a widget. The visible rect is set in AFFECTED-RECT slot, after the widget tree is walked."
  (with-slots (affected-rect) widget
    (list (left affected-rect)
          (top affected-rect)
          (right affected-rect)
          (bottom affected-rect))))

(defun common-bounding-box (bbox1 bbox2)
  (list (min (first bbox1) (first bbox2))
        (min (second bbox1) (second bbox2))
        (max (third bbox1) (third bbox2))
        (max (fourth bbox1) (fourth bbox2))))

(defclass rect ()
  ((left :accessor left :initarg :left :initform 0)
   (top :accessor top :initarg :top :initform 0)
   (width :accessor width :initarg :width :initform 0)
   (height :accessor height :initarg :height :initform 0)))

(gen-print-object rect (left top width height))

(defun right (rect)
  (1- (+ (left rect) (width rect))))

(defun bottom (rect)
  (1- (+ (top rect) (height rect))))

(defclass point ()
  ((x :accessor x :initarg :x :initform 0)
   (y :accessor y :initarg :y :initform 0)))

(gen-print-object point (x y))

(defun corners-of-rectangle (rect)
  (with-slots (left top width height) rect
    (list (make-instance 'point :x left :y top)
          (make-instance 'point :x (1- (+ left width)) :y top)
          (make-instance 'point :x left :y (1- (+ top height)))
          (make-instance 'point :x (1- (+ left width)) :y (1- (+ top height))))))

(defun in-rect (x y rect)
  (with-slots (left top width height) rect
    (and (<= left x)
         (< x (+ left width))
         (<= top y)
         (< y (+ top height)))))

(defun rect-intersect (rect1 rect2)
  (labels ((rect-intersection ()
             (let ((left (max (left rect1) (left rect2)))
                   (top (max(top rect1) (top rect2)))
                   (right (min (right rect1) (right rect2)))
                   (bottom (min (bottom rect1) (bottom rect2))))
               (make-instance 'rect
                              :left left
                              :top top
                              :width (1+ (- right left))
                              :height (1+ (- bottom top)))))
           (intersect-p ()
             (or (dolist (corner (corners-of-rectangle rect1))
                   (if (in-rect (x corner) (y corner) rect2)
                       (return t)))
                 (dolist (corner (corners-of-rectangle rect2))
                   (if (in-rect (x corner) (y corner) rect1)
                       (return t))))))
    (when (intersect-p)
      (rect-intersection))))

(defun layout-rect (widget)
  (make-instance 'rect
                 :left (layout-left widget)
                 :top (layout-top widget)
                 :width (layout-width widget)
                 :height (layout-height widget)))

(defun intersects-clip-rect (widget offset-x offset-y clip-rect)
  (let ((widget-rect (layout-rect widget)))
    (decf (left widget-rect) offset-x)
    (decf (top widget-rect) offset-y)
    (if (null clip-rect)
        t
        (rect-intersect widget-rect clip-rect))))

(defun visible-rect (widget offset-x offset-y clip-rect)
  "Calculates the visible area of WIDGET, taking into account OFFSET values and CLIP region (they are set when the widget is inside a clipped or scrolling widget)."
  (let ((visible-rect (layout-rect widget)))
    (decf (left visible-rect) offset-x)
    (decf (top visible-rect) offset-y)
    (if (null clip-rect)
        visible-rect
        (rect-intersect visible-rect clip-rect))))

(defun assign-paint-numbers (root-widget)
  (let ((number 0)
        (offset-x 0)
        (offset-y 0)
        (clip-rect-stack nil)
        (clipper-stack nil))
    (paint-order-walk root-widget
                      (lambda (object)
                        (setf (paint-order-number object) nil)
                        (when (visible object)
                          ;; AFFECTED-RECT slot is set with the visible area of the widget (taking into account clips and translations)
                          (setf (affected-rect object)
                                (visible-rect object
                                              offset-x offset-y
                                              (car clip-rect-stack)))
                          (when (affected-rect object)
                            (setf (paint-order-number object) number)
                            (incf number)
                            (when (clips-content object)
                              (push (affected-rect object)
                                    clip-rect-stack)
                              (push object clipper-stack))
                            (when (translates-content object)
                              (multiple-value-bind (horizontal-offset vertical-offset) (translation-offsets object)
                                (incf offset-x horizontal-offset)
                                (incf offset-y vertical-offset)))
                            t)))
                      :after-callback (lambda (object)
                                        (when (visible object)
                                          (when (and (eq object (car clipper-stack))
                                                     (translates-content object))
                                            (multiple-value-bind (horizontal-offset vertical-offset) (translation-offsets object)
                                              (decf offset-x horizontal-offset)
                                              (decf offset-y vertical-offset)))
                                          (when (and (eq object (car clipper-stack))
                                                     (clips-content object))
                                            (pop clipper-stack)
                                            (pop clip-rect-stack)))))))

(defun paint-scene (scene)
  (if (or (null (dirty-list scene)) t)
      ;; If there's no dirty-list, then paint the whole scene
      (paint-order-walk (widget scene)
                        (lambda (object)
                          (when (visible object)
                            (paint object)
                            t))
                        :after-callback (lambda (object)
                                          (when (visible object)
                                            (after-paint object))))
      ;; else, paint the dirty widgets
      (progn
        (assign-paint-numbers (widget scene))
        (paint-order-walk (widget scene)
                          (lambda (object)
                            (when (visible object)
                              (cond ((widget-paint-member object (dirty-list scene))
                                     (paint object)
                                     (push object (dirty-list scene)))
                                    ((translates-content object)
                                     (paint object)))
                              t))
                          :after-callback (lambda (object)
                                            (when (visible object)
                                              (cond ((widget-paint-member object
                                                                          (dirty-list scene))
                                                     (after-paint object))
                                                    ((translates-content object)
                                                     (after-paint object))))))
        ;; Set the rectangle to redraw. The common bounding box of all dirty repainted widgets
        (setf (rectangle-to-redraw scene)
              (reduce #'common-bounding-box
                      (mapcar #'visible-bounding-box (dirty-list scene))))))
  (when *inspector-enabled-p*
    (highlight-inspected-widget (inspector scene)))
  (setf (dirty-list scene) nil))

;;-- ** Prepare, measure, layout

;;-- A widget size and positioning consists of three phases.

;;-- First, a widget is "prepared". The *prepare* method is called. The default implementation is to apply styles here, before measuring and layout, because the application of styles can vary the size and position. The *paint-order-walk* function walks the widget hierarchy in "paint order" and prepares all the widgets.

(defmethod prepare ((object scene) scene)
  (paint-order-walk (widget scene)
                    (lambda (w) (prepare w scene))))

;;-- Second, a widget is measured. The result of this phase is that a widget's *measured-width* and *measured-height* should be set. The call to *measure* is accompanied with the current available-width and height of the container widget. The result of this is a call to *set-measured* method with the calculated sizes.
;;-- This is important to understand: the measured width and height is not the definitive size of the widget. Actually, the definitive size of the widget is decided in the third phase, the layout phase. This is because there are container and layout widgets that have the last word on this. For example, a button could have width *X* all alone, and so, it's measured width is set to *X*. But as it belongs to a container that expands its children widgets to fill the remaining space, then in the layout phase of the container, the final width used will be that of the container.
;;-- So, in a way, we can think of the deciding of the final size as a negotiation between the container widget and the widget, in which the ideal or preferred size is calculated in the *measure* method, and the final size is decided in the layout phase.

(defmethod measure ((object scene) available-width available-height)
  (measure (widget object) available-width available-height))

;;-- Finally, in the layout phase, both the position of the widget and its final size is determined. This is done via the *layout* method, that receives the left and top position, the width and the height, and sets the widget layout values via *set-layout*.
;;-- Again, note that the width and height passed to the *layout* method could be not the same to the ones previously calculated by the widget in the measuring phase. The container widget could want some size for the widget (for example, to expand to fill all the space), and usually that is what is used. But here the widget has a final possibility to decide not to use that, and use its measured size instead. Same ocurrs with the left and top position. The passed values are the ones decided by the container, which may differ from those preffered by the widget (for example, if the widget supports absolute positioning).

(defmethod layout ((object scene) left top width height)
  (layout (widget object) left top width height))

(defgeneric handles-mouse-p (object mouse-event)
  (:documentation "Returns true when OBJECT handles MOUSE-EVENT")
  (:method (object mouse-event)
    "Default implementation. Returns T"
    t))

(defun hit-test (widget x y mouse-event)
  "This method iterates over all of the visible subviews to find the most nested view that contains the point."
  (let (result translation-views)
    (paint-order-walk widget
                      (lambda (object)
                        (when (in-widget x y object)
                          (when (translates-content object)
                            (push object translation-views)
                            (multiple-value-bind (horizontal-offset vertical-offset) (translation-offsets object)
                              (incf x horizontal-offset)
                              (incf y vertical-offset)))
                          (when (handles-mouse-p object mouse-event)
                            (setf result object))))
                      :after-callback (lambda (object)
                                        (when (eq (car translation-views) object)
                                          (pop translation-views)
                                          (multiple-value-bind (horizontal-offset vertical-offset)
                                              (translation-offsets object)
                                            (decf x horizontal-offset)
                                            (decf y vertical-offset)))))
    result))

;;-- * Events

(defun cascade-then-bubble (widget-chain event event-arg)
  (dolist (widget widget-chain)
    (when (not (handled event-arg))
      (on-event widget event event-arg :cascade))
    (when (translates-content widget)
      (adjust-event-coordinates event-arg
                                (horizontal-offset widget)
                                (vertical-offset widget))))
  (dolist (widget (reverse widget-chain))
    (when (not (handled event-arg))
      (on-event widget event event-arg :bubble))
    (when (translates-content widget)
      (adjust-event-coordinates event-arg
                                (- (horizontal-offset widget))
                                (- (vertical-offset widget))))))

(defun branch-diff (branch1 branch2)
  (cond ((null branch1) nil)
        ((null branch2) branch1)
        ((eq (first branch1) (first branch2)) (branch-diff (rest branch1)
                                                           (rest branch2)))
        (t branch1)))

(defun calculate-mouse-leave (old-chain new-chain)
  (branch-diff old-chain new-chain))

(defun calculate-mouse-enter (old-chain new-chain)
  (branch-diff new-chain old-chain))

(defun scene-handle-mouse-captors (scene event mouse-event)
  (dolist (captor (mouse-captors scene))
    (setf (handled mouse-event) nil)
    (on-event captor event mouse-event nil)))

(defun scene-on-mouse-move (scene mouse-event)
  (let* ((widget-chain (get-widget-chain
                        (remove nil
                                (list (hit-test (widget scene)
                                                (mouse-x mouse-event)
                                                (mouse-y mouse-event)
                                                mouse-event)))))
         (mouse-leave-widgets (calculate-mouse-leave (last-widget-chain scene)
                                                     widget-chain))
         (mouse-enter-widgets (calculate-mouse-enter (last-widget-chain scene)
                                                     widget-chain)))
    (setf (last-widget-chain scene) widget-chain)
    (cascade-then-bubble mouse-leave-widgets :mouse-leave mouse-event)
    (setf (handled mouse-event) nil)
    (cascade-then-bubble mouse-enter-widgets :mouse-enter mouse-event)
    ;; Inspector code
    (when *inspector-enabled-p*
      (let ((widget (first mouse-enter-widgets)))
        (when widget
          (inspect-widget (inspector scene) widget scene))))
    ;; end inspector code
    (setf (handled mouse-event) nil)
    (cascade-then-bubble widget-chain :mouse-move mouse-event)
    (setf (handled mouse-event) nil)
    (scene-handle-mouse-captors scene :mouse-move mouse-event)))

(defun scene-on-mouse-button (scene mouse-event)
  (let ((widget-chain (get-widget-chain (remove nil
                                                (list (hit-test (widget scene)
                                                                (mouse-x mouse-event)
                                                                (mouse-y mouse-event)
                                                                mouse-event)))))
        (event (if (eq (button-state mouse-event) :down)
                   :mouse-button-down
                   :mouse-button-up)))
    (cascade-then-bubble widget-chain event mouse-event)
    (scene-handle-mouse-captors scene event mouse-event)))

;;-- * Widgets focus

(defun set-focus (scene focus &optional (invalidate t))
  (when (and (< (focused-index scene) (length (focusables scene)))
             (not (eq (has-focus (aref (focusables scene) (focused-index scene)))
                      focus)))
    (setf (has-focus (aref (focusables scene) (focused-index scene)))
          focus)
    (when invalidate
      (invalidate (aref (focusables scene) (focused-index scene))))))

(defun focus-widget (scene widget)
  (let ((widget-pos (position widget (focusables scene))))
    (when (and (focusable widget) widget-pos)
      (cond ((focused-widget scene)
             (unless (eq (focused-widget scene) widget)
               (set-focus scene nil)
               (setf (focused-index scene) widget-pos)
               (set-focus scene t)))
            (t
             (setf (focused-index scene) widget-pos)
             (set-focus scene t))))))

(defun calculate-focusables (scene)
  (when (focusables scene)
    (error "TODO: recalculate focusables, keep the focus on the currently focused object."))
  (let (result)
    (paint-order-walk (widget scene)
                      (lambda (widget)
                        (when (focusable widget)
                          (push widget result))
                        t))
    (setf (focusables scene) (coerce (nreverse result) 'vector))
    (setf (focused-index scene) 0)
    (set-focus scene t nil)))

(defun focused-widget (scene)
  (when (< (focused-index scene) (length (focusables scene)))
    (let ((result (aref (focusables scene) (focused-index scene))))
      (if (has-focus result) result))))

(defun focus-next (scene)
  (set-focus scene nil)
  (incf (focused-index scene))
  (when (= (focused-index scene) (length (focusables scene)))
    (setf (focused-index scene) 0))
  (set-focus scene t))

(defun focus-previous (scene)
  (set-focus scene nil)
  (decf (focused-index scene))
  (when (< (focused-index scene) 0)
    (setf (focused-index scene) (max (1- (length (focusables scene)))
                                     0)))
  (set-focus scene t))

(defun shifted (modifiers)
  (if (consp modifiers)
      (cond ((or (eq (car modifiers) :mod-lshift)
                 (eq (car modifiers) :mod-rshift))
             t)
            (t (shifted (cdr modifiers))))))

(defun scene-on-key (scene key-event)
  (let ((event-kind (if (eq (key-state key-event) :down)
                        :key-down
                        :key-up)))
    (unless (focused-widget scene)
      (on-event (widget scene) event-kind key-event nil))
    (aif (focused-widget scene)
         (let ((widget-chain (get-widget-chain (list it))))
           (cascade-then-bubble widget-chain event-kind key-event)
           (when (not (handled key-event))
             (when (and (eq :key-down event-kind)
                        (eq (key key-event) :tab))
               (setf (handled key-event) t)
               (if (shifted (modifiers key-event))
                   (focus-previous scene)
                   (focus-next scene))))))))

(defmethod initialize-instance :after ((instance scene) &rest initargs)
  (declare (ignore initargs))
  (setf (parent (widget instance)) instance))

(defun prepare-scene (scene)
  (unless (layedout scene)
    (setf (layedout scene) t)
    (prepare scene scene)
    (measure scene (width scene) (height scene))
    (layout scene 0 0 (width scene) (height scene))))
