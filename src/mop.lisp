(in-package :scenic)

(defclass component (scenic:widget)
  ((%builtp :accessor %builtp
            :initform nil)
   (%slot-change-updates :accessor %slot-change-updates
                         :initform t)))

(defclass component-class (standard-class)
  ((%rebuild-on-change :writer (setf %rebuild-on-change)
                       :initarg :rebuild-on-change
                       :initform nil
                       ;; Hack. REmove.
                       ;:type boolean
                       :documentation "If T, then the component is rebuilt when a change occurs")))

;; Hack. Remove
;; Problem is :rebuild-on-change value is given by mop as a list of boolean and not a boolean
(defmethod %rebuild-on-change ((class component-class))
  (let ((slot-value (slot-value class '%rebuild-on-change)))
    (if (listp slot-value)
        (first slot-value)
        slot-value)))

(defmethod closer-mop:validate-superclass
    ((class component-class)
     (superclass standard-class))
  t)

(defclass component-slot-definition ()
  ((child :initarg :child
          :accessor child-slot-p
          :initform nil
          :type boolean
          :documentation "Whether this slot holds a child component")
   (state :initarg :state
          :accessor state-slot-p
          :initform nil
          :type boolean
          :documentation "Whether this slot is part of the component's state")
   (prop :initarg :prop
         :accessor prop-slot-p
         :initform nil
         :type boolean
         :documentation "Whether this slot holds a component property")
   (on-change :initarg :on-change
              :accessor slot-on-change
              :initform nil
              :documentation "What to do when the slot changes its value. A list of :invalidate :layout :rebuild")))

(defmethod child-slot-p (slot)
  nil)

(defmethod state-slot-p (slot)
  nil)

(defmethod prop-slot-p (slot)
  nil)

(defclass component-direct-slot-definition
    (component-slot-definition closer-mop:standard-direct-slot-definition)
  ())

(defclass component-effective-slot-definition
    (component-slot-definition closer-mop:standard-effective-slot-definition)
  ())

(defmethod closer-mop:direct-slot-definition-class
    ((class component-class)
     &rest initargs)
  (declare (ignore initargs))
  (find-class 'component-direct-slot-definition))

(defmethod closer-mop:effective-slot-definition-class
    ((class component-class)
     &rest initargs)
  (declare (ignore initargs))
  (find-class 'component-effective-slot-definition))

(defmethod closer-mop:compute-effective-slot-definition ((class component-class) name direct-slots)
  (let ((effective-slot (call-next-method)))
    (setf (child-slot-p effective-slot)
          (some #'child-slot-p direct-slots))
    (setf (state-slot-p effective-slot)
          (some #'state-slot-p direct-slots))
    (setf (prop-slot-p effective-slot)
          (some #'prop-slot-p direct-slots))

    (dolist (slot direct-slots)
      (when (typep slot 'component-slot-definition)
        (setf (slot-on-change effective-slot) 
              (slot-on-change slot))
        (return)))    
    effective-slot))

(defmethod shared-initialize :around ((class component-class) slot-names &rest args &key direct-superclasses)
  "Ensures we inherit from component."
  (let* ((component-metaclass (find-class 'component-class))
         (component-class (find-class 'component))
         (not-already-component
          (loop for superclass in direct-superclasses
             never (eq (class-of superclass) component-metaclass))))
    (if (and (not (eq class component-class)) not-already-component)
        (apply #'call-next-method class slot-names
               :direct-superclasses (append direct-superclasses (list component-class)) args)
        (call-next-method))))

(defmethod initialize-instance :around ((comp component) &rest initargs)
  "Disable slot updates on initialization"
  (setf (%slot-change-updates comp) nil)
  (call-next-method)
  (setf (%slot-change-updates comp) t))

(defmethod child-slots ((obj component))
  (child-slots (class-of obj)))

(defmethod child-slots ((class component-class))
  (remove-if-not #'child-slot-p (closer-mop:class-slots class)))

(defmethod state-slots ((obj component))
  (state-slots (class-of obj)))

(defmethod state-slots ((class component-class))
  (remove-if-not #'state-slot-p (closer-mop:class-slots class)))

(defmethod prop-slots ((obj component))
  (prop-slots (class-of obj)))

(defmethod prop-slots ((class component-class))
  (remove-if-not #'prop-slot-p (closer-mop:class-slots class)))

(defmethod (setf closer-mop:slot-value-using-class) :after
    (new-value
     (class component-class)
     (object component)
     (slot component-slot-definition))
  (when (%slot-change-updates object)
    (when (%rebuild-on-change class)
      (setf (%builtp object) nil))
    (when (member :invalidate (slot-on-change slot))
      (scenic::invalidate object))
    (when (member :layout (slot-on-change slot))
      (scenic::mark-for-layout object))
    (when (member :rebuild (slot-on-change slot))
      (setf (%builtp object) nil)
      (scenic::mark-for-layout object)
      (scenic::invalidate object))
    (when (child-slot-p slot)
      (when (typep new-value 'scenic:widget)
        (setf (scenic::parent new-value) object)))
    (when (state-slot-p slot)
      (scenic::invalidate object))))

(defgeneric build (component)
  (:method ((component component)))
  (:method :after ((component component))
           (setf (%builtp component) t)))

(defmethod rebuild ((component component))
  ;; remove-all-children: Should this be optional? A metaclass option?
  (remove-all-children component)
  ;; end comment
  (build component))

(defmethod scenic::measure :before ((component component) available-width available-height)
  "TODO: maybe put this somewhere else in the update cycle (example: create a prepare-for-render method on widgets)"
  (when (not (%builtp component))
    (build component)))

;; Default implementations of measure and layout??
;; (defmethod scenic:measure ((component component) available-width available-height)
;;   (multiple-value-bind (width height)
;;       (scenic:measure (%child tab-view) available-width available-height)
;;     (set-measured tab-view width height)))

;; (defmethod scenic::layout ((object component) left top width height)
;;   (scenic::layout (%child object)
;;           left
;;           top
;;           width
;;           height)
;;   (scenic::set-layout object left top width height))

(defmethod scenic:measure ((component component) available-width available-height)
  (error "Implement scenic:measure for ~A" component))

(defmethod scenic:layout ((component component) left top width height)
  (error "Implement scenic:layout for ~A" component))                           

(defmethod remove-all-children ((comp component))
  (dolist (slot (child-slots (class-of comp)))
    (let ((slot-name (closer-mop:slot-definition-name slot)))
      (setf (slot-value comp slot-name) nil))))

(defmethod component-children ((comp component))
  (remove nil
          (mapcar (lambda (slot)
                    (let ((slot-name (closer-mop:slot-definition-name slot)))
                      (slot-value comp slot-name)))
                  (child-slots (class-of comp)))))

(defmethod scenic::paint-order-walk ((object component) callback &key (after-callback nil))
  (when (funcall callback object)
    (dolist (child (component-children object))
      (when child
        (scenic::paint-order-walk child callback :after-callback after-callback))))
  (when after-callback
    (funcall after-callback object)))

(defmethod scenic::paint ((comp component))
  )

(defmethod scenic::fill-props progn ((comp component) values)
  (dolist (slot (prop-slots comp))
    (let ((prop (alexandria:make-keyword (closer-mop:slot-definition-name slot))))
    (scenic::apply-style-property
     prop
     (access:access values prop)
     comp))))

(defmacro defcomponent (name superclasses slots &rest options)
  `(defclass ,name ,superclasses
     ,slots
     ,@(cons (list :metaclass 'component-class) options)))      

#+test
(defcomponent my-component ()
  ((active-item :initform nil
                :accessor active-item
                :child t
                :on-change (:invalidate :layout)))
  (:rebuild-on-change t))
