(in-package :scenic)

;;; PADDING class.

(defclass padding (container1)
  ((padding-left :accessor padding-left :initarg :padding-left :initform 0)
   (padding-top :accessor padding-top :initarg :padding-top :initform 0)
   (padding-right :accessor padding-right :initarg :padding-right :initform 0)
   (padding-bottom :accessor padding-bottom :initarg :padding-bottom :initform 0)))

(defmethod measure ((object padding) available-width available-height)
  (multiple-value-bind (child-width child-height)
      (measure (child object)
               (- available-width
                  (padding-left object) (padding-right object))
               (- available-height
                  (padding-top object) (padding-bottom object)))
    (let ((width (+ (padding-left object) (padding-right object) child-width))
          (height (+ (padding-top object) (padding-bottom object) child-height)))
      (set-measured object width height))))

(defmethod layout ((object padding) left top width height)
  (layout (child object)
          (+ left (padding-left object))
          (+ top (padding-top object))
          (- width (+ (padding-left object) (padding-right object)))
          (- height (+ (padding-top object) (padding-bottom object))))
  (set-layout object left top width height))

;;; BORDER class.

(defclass border (container1)
  ((stroke-color :accessor stroke-color :initarg :stroke-color :initform 0)
   (stroke-width :accessor stroke-width :initarg :stroke-width :initform 0)
   (corner-radius :accessor corner-radius :initarg :corner-radius :type (or null number) :initform nil)))

(defmethod paint ((object border))
  (scenic-utils::draw-rect (layout-left object)
                           (layout-top object)
                           (layout-width object)
                           (layout-height object)
                           :stroke-color (stroke-color object)
                           :stroke-width (stroke-width object)
                           :corner-radius (corner-radius object)))

(defmethod measure ((object border) available-width available-height)
  (multiple-value-bind (child-width child-height)
      (measure (child object)
               (- available-width (* 2 (stroke-width object)))
               (- available-height (* 2 (stroke-width object))))
    (let ((width (+ (* 2 (stroke-width object)) child-width))
          (height (+ (* 2 (stroke-width object)) child-height)))
      (set-measured object width height))))

(defmethod layout ((object border) left top width height)
  (layout (child object)
          (+ left (stroke-width object))
          (+ top (stroke-width object))
          (- width (* 2 (stroke-width object)))
          (- height (* 2 (stroke-width object))))
  (set-layout object left top width height))

;;; BACKGROUND class.

(defclass background (container1)
  ((fill-color :accessor fill-color :initarg :fill-color :initform 0)))

(defmethod paint ((object background))
  (scenic.draw:rectangle (layout-left object)
                         (layout-top object)
                         (layout-width object)
                         (layout-height object))
  (scenic.draw:set-color (fill-color object))
  (scenic.draw:fill-path))

;;; SIZER class.

(defclass sizer (container1)
  ((min-width :accessor min-width :initarg :min-width :initform nil)
   (min-height :accessor min-height :initarg :min-height :initform nil)
   (max-width :accessor max-width :initarg :max-width :initform nil)
   (max-height :accessor max-height :initarg :max-height :initform nil)))

(defmethod measure ((object sizer) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object)
               (if (null (max-width object))
                   available-width
                   (min available-width (max-width object)))
               (if (null (max-height object))
                   available-height
                   (min available-height (max-height object))))
    (when (not (null (min-width object)))
      (setf width (max width (min-width object))))
    (when (not (null (min-height object)))
      (setf height (max height (min-height object))))
    (when (not (null (max-width object)))
      (setf width (min width (max-width object))))
    (when (not (null (max-height object)))
      (setf height (min height (max-height object))))
    (set-measured object width height)))

(defmethod layout ((object sizer) left top width height)
  (if (min-width object)
      (setf width (max width (min-width object))))
  (if (max-width object)
      (setf width (min width (max-width object))))
  (if (min-height object)
      (setf height (max height (min-height object))))
  (if (max-height object)
      (setf height (min height (max-height object))))
  (layout (child object) left top width height)
  (set-layout object left top width height))

(defmethod (setf width) (value (sizer sizer))
  (setf (min-width sizer) value
        (max-width sizer) value))

(defmethod (setf height) (value (sizer sizer))
  (setf (min-height sizer) value
        (max-height sizer) value))  

;;; ALIGNER class.

(defclass aligner (container1)
  ((vertical :accessor vertical :initarg :vertical :initform :center)
   (horizontal :accessor horizontal :initarg :horizontal :initform :center)))

(defmethod layout ((object aligner) left top width height)
  (let ((child-width (measured-width (child object)))
        (child-height (measured-height (child object))))
    (let ((h-space (max 0 (- width child-width)))
          (v-space (max 0 (- height child-height))))
      (layout (child object)
              (ecase (horizontal object)
                ((:left :fill) left)
                ((:right) (+ h-space left))
                ((:center) (+ (/ h-space 2) left)))
              (ecase (vertical object)
                ((:top :fill) top)
                ((:bottom) (+ v-space top))
                ((:center) (+ (/ v-space 2) top)))
              (ecase (horizontal object)
                ((:left :right :center) child-width)
                ((:fill) width))
              (ecase (vertical object)
                ((:top :bottom :center) child-height)
                ((:fill) width)))
      (set-layout object left top width height))))

;;; CLIPPER class.

(defclass clipper (container1)
  ())

(defmethod clips-content ((object clipper))
  t)

(defmethod paint ((instance clipper))
  (scenic.draw:save)
  (scenic.draw:rectangle (layout-left instance)
                         (layout-top instance)
                         (layout-width instance)
                         (layout-height instance))
  (scenic.draw:clip))

(defmethod after-paint ((instance clipper))
  (scenic.draw:restore))

(defclass circular-clipper (clipper)
  ())

(defmethod paint ((instance circular-clipper))
  (scenic.draw:save)
  (let ((xc (/ (- (layout-width instance)
                  (layout-left instance))
               2))
        (yc (/ (- (layout-height instance)
                  (layout-top instance))
               2))
        (r (/
            (min (layout-width instance)
                 (layout-height instance))
            2)))
    (scenic.draw:arc xc yc r 0 (* 2 pi)))
  (scenic.draw:clip))

;;; GLASS class.

(defclass glass (container1)
  ((opacity :accessor opacity :initarg :opacity :initform :center)
   (old-context :accessor old-context
                :initarg :old-context
                :initform :center)
   (cairo-surface :accessor cairo-surface
                  :initarg :cairo-surface
                  :initform :center)))

(defmethod clips-content ((object glass))
  t)

(defmethod paint ((instance glass))
  (setf (cairo-surface instance)
        (cairo:create-image-surface :argb32
                                    ;; We convert possible float to integer with round function here
                                    (round (measured-width instance))
                                    (round (measured-height instance))))
  (setf (old-context instance) cairo:*context*)
  (setf cairo:*context*
        (cairo:create-context (cairo-surface instance)))
  (cairo:translate (- (layout-left instance)) (- (layout-top instance))))

(defmethod after-paint ((instance glass))
  (cairo:destroy cairo:*context*)
  (setf cairo:*context* (old-context instance))

  (cairo:save)
  (cairo:rectangle (layout-left instance)
                   (layout-top instance)
                   (layout-width instance)
                   (layout-height instance))
  (cairo:clip)
  (cairo:set-source-surface (cairo-surface instance)
                            (layout-left instance)
                            (layout-top instance))
  (cairo:paint-with-alpha (opacity instance))
  (cairo:restore)

  ;; Cleaning up.
  (cairo:destroy (cairo-surface instance)))

;;-- PLACER class

(defclass placer (container1)
  ((left :initarg :left :accessor left :initform nil :type (or null number))
   (top :initarg :top :accessor top :initform nil :type (or null number))))

(defmethod layout ((object placer) left top width height)
  (layout (child object)
          (or (left object) left)
          (or (top object) top)
          (measured-width object)
          (measured-height object))
  (set-layout object (or (left object) left)
              (or (top object) top)
              (measured-width object)
              (measured-height object)))

(defun placer (left top child)
  (make-instance 'placer :left left :top top :child child))

;;-- ADJUSTER class

(defclass adjuster (container1)
  ()
  (:documentation "Ignores container layout"))

(defmethod layout ((object adjuster) left top width height)
  (layout (child object)
          left
          top
          (measured-width object)
          (measured-height object))
  (set-layout object
              left
              top
              (measured-width object)
              (measured-height object)))

(defun adjuster (child)
  (make-instance 'adjuster :child child))

;;-- EXPANDER class

(defclass expander (container1)
  ((expand-width :initarg :expand-width
                 :initform t
                 :accessor expand-width-p
                 :type boolean)
   (expand-height :initarg :expand-height
                  :initform t
                  :accessor expand-height-p
                  :type boolean))
  (:documentation "Expands to available space"))

(defmethod layout ((object expander) left top width height)
  (layout (child object)
          left
          top
          width
          height)
  (set-layout object
              left
              top
              width
              height))

(defmethod measure ((object expander) available-width available-height)
  (multiple-value-bind (child-width child-height)
      (measure (child object) available-width available-height)
    (set-measured object
                  (or (and (expand-width-p object) available-width)
                      child-width)
                  (or (and (expand-height-p object) available-height)
                      child-height))))

(defun expander (child &key (expand-width t) (expand-height t))
  (make-instance 'expander
                 :child child
                 :expand-width expand-width
                 :expand-height expand-height))

;;-- TRANSLATOR class

(defclass translator (container1)
  ((horizontal-offset :accessor horizontal-offset
                      :initarg :horizontal-offset
                      :initform 0)
   (vertical-offset :accessor vertical-offset
                    :initarg :vertical-offset
                    :initform 0))
  (:documentation "Translates its child widget horizontally and vertically"))

(defmethod translates-content ((object translator))
  t)

(defmethod translation-offsets ((object translator))
  (values (- (horizontal-offset object))
          (- (vertical-offset object))))

(defmethod paint ((object translator))
  (scenic.draw:save)
  (scenic.draw:translate (horizontal-offset object)
                         (vertical-offset object)))

(defmethod after-paint ((object translator))
  (scenic.draw:restore))

(defun translator (child &key x y)
  (make-instance 'translator
                 :child child
                 :horizontal-offset (or x 0)
                 :vertical-offset(or y 0)))

;;-- SCALER class

(defclass scaler (container1)
  ((x-scale :initarg :x-scale
            :initform 1
            :accessor x-scale
            :type float)
   (y-scale :initarg :y-scale
            :initform 1
            :accessor y-scale
            :type float))
  (:documentation "Scales its child widget"))

(defmethod paint ((object scaler))
  (scenic.draw:save)
  (scenic.draw:scale (x-scale object) (y-scale object)))

(defmethod after-paint ((object scaler))
  (scenic.draw:restore))

(defun scaler (child &key x y)
  (make-instance 'scaler
                 :child child
                 :x-scale (or x 1)
                 :y-scale (or y 1)))

;;-- ROTATOR class

(defclass rotator (container1)
  ((angle :initarg :angle
          :accessor angle
          :type float))
  (:documentation "Rotates its child widget"))

(defmethod paint ((object rotator))
  (scenic.draw:save)
  (let ((x (+ (layout-left object) (/ (layout-width object) 2)))
        (y (+ (layout-top object) (/ (layout-height object) 2))))
  (scenic.draw:translate x y)
  (scenic.draw:rotate (angle object))
  (scenic.draw:translate (- x)
                         (- y))))

(defmethod after-paint ((object rotator))
  (scenic.draw:restore))

(defun rotator (angle child)
  (make-instance 'rotator
                 :child child
                 :angle angle))
