(ql:quickload :sdl2-image)

(in-package :scenic)

(defclass sdl-image (image)
  ())

(defmethod paint ((instance sdl-image))
  (sdl2:blit-surface
   (image-surface instance)
   (sdl2:make-rect
    0 0
    ;;(sdl2:surface-width (image-surface instance))
    (layout-width instance)
    ;;(sdl2:surface-height (image-surface instance))
    (layout-height instance))
   (sdl-surface (get-scene instance))
   (sdl2:make-rect (layout-left instance)
                   (layout-top instance)
                   (layout-width instance)
                   (layout-height instance))))

(defun sdl-image (path)
  (make-instance 'sdl-image
                 :image-surface (sdl2-image:load-image path)))

(defun view-sdl-image (filename)
  (let ((c (stack (background cl-colors:+white+
                              (filler))
                  (sizer (sdl-image (asdf:system-relative-pathname
                                     :scenic (format nil "demo/photos/~A" filename)))
                         :width 600 :height 400))))
    (run-scene (scene 800 600 c))))

;;(view-sdl-image "woman.jpg")
;;(view-sdl-image "tiger.jpg")
;;(view-sdl-image "aircraft.jpg")
;;(view-sdl-image "sea.jpg")
