#|

@title Scenic
@subtitle Simple UI framework

@author Mariano Montone
@syntax org
@short-comments-prefix ;;--

|#

;;-- * Introduction


;;-- *Scenic* is a simple UI framework

;;-- The framework motivations are:
;;-- - Be simple. The whole framework should be small as possible, and also be easy to understand. Ideally, the literate documentation will help with the understanding of its internals.
;;-- - Be composable and flexible. I think it is fair to say that we want Scenic to be more composable than other desktop GUI toolkits. That mostly means not to restrict the content of widgets to a string or an arbitrary thing, but let the content be a collection of components. For example, instead of a button having a string label with an optional icon, let the content of a button be formed by other components. Same for lists. In general, list items are assumed to hold a string. Instead of that, let list items hold whatever components they need. We want to apply this principle to all the framework components. This is very similar to what HTML does, it allows that level of composition. Also, provide a generic block widget for which several properties can be specified, like padding, border, fill color, etc. That makes the framework more flexible (a lot like an HTML div element).
;;-- - Be themable. As what is considered good looking changes with time and fashion, but also because different style of UI are appropiate for different types of applications, we want the looks to be easy to change and adaptable as possible. Renderers and styles should help with that.
;;-- - Cross-platform. Widgets are programmed all from scratch in Lisp and behave the same in all platforms.

;;-- For later, and perhaps:
;;-- - Run on mobile. That would allow Common Lisp applications with no dependency on platform UI.
;;-- - Several backends. It runs on SDL now, but would be nice if could make it run on Mezzano, for example.

(in-package :scenic)

;;-- ** Global variables

(defvar *event-recording-enabled* nil)
(defvar *session-record* nil)

(defvar *test-channel-enabled* nil)
(defvar *scene*)
(defvar *replay-task-timeout-ms* 5000)
(defvar *inspector-enabled-p* nil)


;;-- ** Events recording
;;-- When *event-recording-enabled*, all events are logged into *session-record* for inspection

(defun reset-session-record ()
  (when (or *event-recording-enabled* *test-channel-enabled*)
    (setf *session-record* nil)))

(defun record-events (event-queue)
  (when *event-recording-enabled*
    (push (list* 'events (mapcar #'serialize event-queue))
          *session-record*)))

(defun test-channel-write (data)
  ;;(check-ui-thread)
  (when *test-channel-enabled*
    (push (cons 'test-channel data) *session-record*)))

(defun record-scene (scene)
  (let ((*event-recording-enabled* t)
        (*test-channel-enabled* t))
    (run-scene scene)
    (setf *session-record* (reverse *session-record*))))

(defun break-by (list pred &optional acc)
  (cond ((null list) (values (reverse acc) list))
        ((funcall pred (first list))
         (break-by (rest list) pred (push (first list) acc)))
        (t (values (reverse acc) list))))

(defun test-render-scene (scene &optional force-repaint-all)
  (when (dirty scene)
    (setf (dirty scene) nil)
    (let* ((cairo-surface (cairo:create-image-surface :argb32
                                                      (width scene) (height scene)))
           (cairo:*context* (cairo:create-context cairo-surface)))
      (cairo:destroy cairo-surface)
      (prepare-scene scene)
      (when force-repaint-all
        (invalidate-scene scene))
      (paint-scene scene)
      (cairo:destroy cairo:*context*))))

(defun get-first-diff (actual expected &optional (line 1))
  (cond ((and (null actual) (null expected))
         nil)
        ((null actual) (list "Expected output has extra item." line (car expected)))
        ((null expected) (list "Actual output has extra item." line (car actual)))
        ((equal (car actual) (car expected))
         (get-first-diff (cdr actual) (cdr expected) (1+ line)))
        (t (list "Expected and actual items differ." line (car expected) (car actual)))))

;;-- Session of events can be replayed using replay-scene-session function for testing purposes

(defun replay-scene-session (scene session-record)
  (labels ((run-compare (test-replies action)
             (reset-session-record)
             (funcall action)
             (when (not (equal (reverse *session-record*) test-replies))
               (return-from replay-scene-session
                 (values nil
                         (get-first-diff (reverse *session-record*) test-replies))))
             (reset-session-record)))
    (let ((*test-channel-enabled* t)
          (*event-recording-enabled* nil)
          event-queue
          test-replies)

      (setf (values test-replies session-record)
            (break-by session-record
                      (lambda (elem) (eq (car elem) 'test-channel))))

      (run-compare test-replies (lambda ()
                                  (init-scene scene #'test-render-scene)))

      (loop
         (unless session-record
           (return-from replay-scene-session (values t nil)))

         (unless (eq 'events (caar session-record))
           (return-from replay-scene-session (values nil
                                                     "Expected event in session record.")))
         (setf event-queue (mapcar (lambda (args)
                                     (apply #'make-instance args))
                                   (cdar session-record)))
         (setf session-record (cdr session-record))
         (setf (values test-replies session-record)
               (break-by session-record
                         (lambda (elem) (eq (car elem) 'test-channel))))
         (run-compare test-replies
                      (lambda () (handle-events scene event-queue #'test-render-scene)))))))

;;-- ** Events handling

(defmethod handle-events ((scene scene) event-queue renderer)
  (when event-queue
    (record-events event-queue)
    (dolist (event event-queue)
      (handle-event event scene renderer))))

(defgeneric handle-event (event scene renderer))

(defmethod handle-event ((event mouse-move-event) scene renderer)
  (scene-on-mouse-move scene event)
  (funcall renderer scene))

(defmethod handle-event ((event mouse-button-event) scene renderer)
  (cond
    ((eq (button-state event) :down)
     (scene-on-mouse-button scene event))
    ((eq (button-state event) :up)
     (scene-on-mouse-button scene event)))
  (funcall renderer scene))

(defmethod handle-event ((event key-event) scene renderer)
  (cond
    ((eq (key-state event) :down)
     (scene-on-key scene event))
    ((eq (key-state event) :up)
     (scene-on-key scene event)))
  (funcall renderer scene))

(defmethod handle-event ((event ui-task-executed-event) scene renderer)
  ;; A task is being replayed here. Not executed for the first time.
  ;; pick-task is only used for replaying tasks.
  (pick-ui-task (task-number event) *replay-task-timeout-ms*))

;;-- ** Scene initialization

(defun init-scene (scene renderer)
  (calculate-focusables scene)
  (reset-session-record)
  (reset-ui-tasks)
  (reset-threads)
  (funcall renderer scene t)
  (when (on-scene-init scene)
    (funcall (on-scene-init scene))))

;;-- @include scene.lisp
;;-- @include containers.lisp
;;-- @include renderer.lisp

;; Disable style warnings from CFFI (Cairo package triggers deprecated warnings I don't want to fix right now)
#+sbcl(sb-ext:without-package-locks

        (defun alexandria:simple-style-warning (&rest args)
          )
        )

#-sbcl(defun alexandria:simple-style-warning (&rest args)
        )
