(defpackage :scenic-templates
  (:nicknames :scenic.wt)
  (:use :cl))

(in-package :scenic.wt)

;;-- * Widget templates
;;--
;;-- Widget Templates are a functional description of the user interface.
;;-- Instead of working with widgets directly, the developer works with an immutable representation of the state of the view (widgets layout) based on some status.
;;-- This is very similar to how React and Flutter and other Web GUI frameworks work.

(defclass widget-template ()
  ((key :initarg :key :accessor wt-key :initform nil)
   (events :initarg :events :accessor wt-events :initform nil))
  (:documentation "A widget-template is a representation of a configuration of a widget."))

(defmethod print-object ((wt widget-template) stream)
  (print-unreadable-object (wt stream :type t)
    (format stream "#~A" (wt-key wt))))

(defstruct wt-prop
  name
  initarg
  accessor
  reader
  writer
  test)

(defstruct wt-event
  name
  handler)

(defclass wt-container (widget-template)
  ((children :initarg :children :accessor wt-children)))

(defclass wt-container1 (widget-template)
  ((child :initarg :child :accessor wt-child)))

(defmethod wt-children ((wt wt-container1))
  (list (wt-child wt)))

(defclass wt-child (widget-template)
  ())

(defclass wt-renderer ()
  ()
  (:documentation "Widget templates are rendered from widget template renderers (wt-renderers). wt-renderers form a tree and hold state. The result of the rendering is a tree of widget templates"))

(defgeneric render (wt-renderer)
  (:method (wt-renderer)
    (error "Rendering method has not been implemented for renderer: ~A" wt-renderer)))

(defgeneric wt-props (wt)
  (:method ((wt widget-template))
    nil))

(defmacro def-widget-template (name super props &rest options)
  (let ((widget-class (cadr (find :widget-class options :key 'car)))
        (events (cdr (find :events options :key 'car))))
    `(progn
       (defclass ,name ,super
         ,(mapcar (lambda (prop)
                    `(,(first prop)
                       ,@(when (getf (rest prop) :initarg)
                           (list :initarg (getf (rest prop) :initarg)))
                       ,@(when (getf (rest prop) :accessor)
                           (list :accessor (getf (rest prop) :accessor)))))
                  props))
       (defmethod wt-props ((,name ,name))
         (append (mapcar (lambda (prop)
                           (apply #'make-wt-prop :name (first prop)
                                  (rest prop)))
                         ',props)
                 (call-next-method)))       
       (defmethod wt-widget-class ((,name ,name))
         ',widget-class))))

#+nil(defun set-prop (wt prop w)
  (funcall (or (wt-prop-writer prop)
               (fdefinition `(setf ,(wt-prop-accessor prop))))
           (slot-value wt (wt-prop-name prop))
           w))

(defun set-prop (w prop val)
  (funcall
   (or (wt-prop-writer prop)
       (fdefinition `(setf ,(wt-prop-accessor prop))))
   val
   w))

(defun get-prop (w prop)
  (funcall (or (wt-prop-reader prop)
               (fdefinition (wt-prop-accessor prop)))
           w))

(defun update-prop (w prop new-val old-val)
  (let ((prop-test (or (wt-prop-test prop)
                       #'equalp)))
    (when (not (funcall prop-test new-val old-val))
      (set-prop w prop new-val))))

(defun update-props (w new-node old-node)
  (loop for prop in (wt-props new-node)
     do (update-prop w prop
                     (get-prop new-node prop)
                     (get-prop old-node prop))))                     

(defgeneric wt-create-widget (wt)
  (:documentation "WT-CREATE-WIDGET is in charge of creating the widget tree from the template representation"))

(defmethod wt-create-widget (wt)
  (let ((w
         (apply #'make-instance (wt-widget-class wt)
                (alexandria:flatten
                 (mapcar (lambda (prop)
                           (when (slot-boundp wt (wt-prop-name prop))
                             (list (wt-prop-initarg prop)
                                   (slot-value wt (wt-prop-name prop)))))
                         (wt-props wt))))))
    (loop for ev in (wt-events wt)
       do (scenic:add-event-handler w (car ev) nil (cdr ev)))
    w))

(defmethod wt-create-widget ((wt wt-container))
  (let ((w (call-next-method)))
    (setf (slot-value w 'scenic:children)
          (mapcar (lambda (wt-child)
                    (let ((child (wt-create-widget wt-child)))
                      (setf (scenic:parent child) w)
                      child))
                  (wt-children wt)))
    w))

(defmethod wt-create-widget ((wt wt-container1))
  (let ((w (call-next-method)))
    (let ((child (wt-create-widget (wt-child wt))))
      (setf (slot-value w 'scenic:child) child)
      (setf (scenic:parent child) w)
      w)))

(defun wt-changed-p (wt1 wt2)
  (or
   (not (eql (type-of wt1) (type-of wt2)))
   (not (eql (wt-key wt1) (wt-key wt2)))))

(defun wt-diff (wt1 wt2)
  "Calculates the differences between the two widget templates WT1 and WT2 and returns a list of patches to be applied to the widget tree")

(defun wt-update (parent new-node old-node &optional (index 0))
  "Similar to wt-diff, but updates the widget tree W directly instead of producing patches"
  (cond
    ((not old-node)
     ;; new-node is not in old tree. add it.
     (scenic::add-child parent (wt-create-widget new-node))
     ;;(format t "TEMPLATE: ADD CHILD~%")
     )
    ((not new-node)
     ;; there's no new node. remove from tree.
     (scenic::remove-child parent index)
     ;;(format t "TEMPLATE: REMOVE CHILD~%")
     )
    ((wt-changed-p new-node old-node)
     ;; if there are both new-node and old-node, but they are different, replace.
     ;;(format t "TEMPLATE: REPLACE CHILD: ~A ~A~%"
     ;;        new-node old-node)
     (cond
       ((typep parent 'wt-scene)
        (setf (scenic::widget parent) (wt-create-widget new-node)))
       ((typep parent 'scenic::container1)
        (setf (scenic:child parent) (wt-create-widget new-node)))
       (t
        (scenic::replace-child parent index (wt-create-widget new-node)))))
    ;; there are two compatible nodes which have not "changed". still we have to look at their properties and children.
    ((typep new-node 'wt-container)
     (let ((target (cond
                     ((typep parent 'wt-scene)
                      (scenic::widget parent))
                     ((typep parent 'scenic::container1)
                      (scenic:child parent))
                     (t
                       (nth index (scenic:children parent))))))
       ;; update props
       (update-props target new-node old-node)
       ;; update children
       (loop for i from 0 to (1- (max (length (wt-children new-node))
                                      (length (wt-children old-node))))
          do (wt-update target
                        (nth i (wt-children new-node))
                        (nth i (wt-children old-node))
                        i))))
    ((typep new-node 'wt-container1)
     (let ((target (cond
                     ((typep parent 'wt-scene)
                      (scenic::widget parent))
                     ((typep parent 'scenic::container1)
                      (scenic:child parent))
                     (t
                       (nth index (scenic:children parent))))))
       ;; update props
       (update-props target new-node old-node)
       ;; update child
       (wt-update target
                  (wt-child new-node)
                  (wt-child old-node))))
    (t
     ;;(format t "No change")
     )))

(def-widget-template wt-button (wt-container1)
  ()
  (:widget-class scenic:button))

(def-widget-template wt-label (wt-child)
  ((text :initarg :text :accessor scenic::text :test string=))
  (:widget-class scenic:label))

(def-widget-template wt-pack (wt-container)
  ((space-between-cells :initarg :space-between-cells
                        :accessor scenic::space-between-cells
                        :test eql)
   (orientation :initarg :orientation
                :accessor scenic::orientation))
  (:widget-class scenic:pack))

(def-widget-template wt-background (wt-container1)
  ((fill-color :initarg :fill-color :accessor scenic:fill-color))
  (:widget-class scenic:background))

(defun extract-events (args)
  (let (rest-args events)
    (loop
       for key in args by #'cddr
       for val in (rest args) by #'cddr
       do (if (and (>= (length (string key)) 3)
                   (equalp (subseq (string key) 0 3) "ON-"))
              (push (cons (alexandria:make-keyword (subseq (string key) 3))
                          val)
                    events)
              (progn
                (push key rest-args)
                (push val rest-args))))
    (values events (reverse rest-args))))         

(defun button (child &rest args)
  (multiple-value-bind (events args)
      (extract-events args)
    (apply #'make-instance 'wt-button :child child :events events args)))
     
(defun label (text &rest args)
  (apply #'make-instance 'wt-label :text text args))

(defun simple-vertical-pack (space children)
  (make-instance 'wt-pack
                 :space-between-cells space
                 :orientation :vertical
                 :children children))

(defun background (color child)
  (make-instance 'wt-background
                 :fill-color color
                 :child child))

(defclass wt-scene (scenic:scene)
  ((wt-renderer :initarg :wt-renderer
             :initform (error "Provide the widget templates renderer")
             :accessor scene-wt-renderer)
   (w-template :initarg :w-template
             :initform nil
             :accessor scene-w-template
             :documentation "The scene current widget template")))

(defun wt-scene (width height wt-renderer &rest initargs)
  (let ((wt (render wt-renderer)))
    (apply #'make-instance 'wt-scene
           :width width
           :height height
           :wt-renderer wt-renderer
           :w-template wt
           :widget (wt-create-widget wt)
           initargs)))

(defmethod scenic::handle-events :after ((scene wt-scene) event-queue renderer)
  (let ((new-template (render (scene-wt-renderer scene))))
    (wt-update scene
               new-template
               (scene-w-template scene))
    (setf (scene-w-template scene) new-template)))

(defclass wt-example-1 (wt-renderer)
  ((selected-item :initform :opt1 :accessor selected-item)))

(defmethod render ((renderer wt-example-1))
  (background cl-colors:+white+
              (simple-vertical-pack
               5
               (list (button (label "Switch")
                             :on-click
                             (lambda (btn ev)
                               (setf (selected-item renderer)
                                     (case (selected-item renderer)
                                       (:opt1 :opt2)
                                       (:opt2 :opt1)))
                               (format t "Selected: ~A~%" (selected-item renderer))
                               ))
                     (ecase (selected-item renderer)
                       (:opt1 (label "Option 1" :key :opt1))
                       (:opt2 (label "Option 2" :key :opt2)))))))

(defun wt-example-1 ()
  "(wt-example-1)"
  (let ((scene (wt-scene 500 500 (make-instance 'wt-example-1))))
    (scenic:run-scene scene)))
