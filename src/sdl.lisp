(in-package :scenic)

(defmacro pixels-of (surface)
  `(cffi:foreign-slot-value (slot-value ,surface 'sdl::foreign-pointer-to-object)
                            'sdl-cffi::sdl-Surface
                            'sdl-cffi::pixels))

(defmacro pitch-of (surface)
  `(cffi:foreign-slot-value (slot-value ,surface 'sdl::foreign-pointer-to-object)
                            'sdl-cffi::sdl-Surface
                            'sdl-cffi::pitch))

(defun render-scene (scene &optional force-repaint-all)
  (unless (sdl-surface scene)
    (setf (sdl-surface scene)
          (sdl:create-surface (width scene) (height scene)
                              :mask '(#x00ff0000 #x0000ff00 #x000000ff 0))))
  (when (dirty scene)
    (setf (dirty scene) nil)

    (scenic.draw:with-drawing-context (sdl-surface scene)
      (prepare-scene scene)
      (when force-repaint-all
        (invalidate-scene scene))
      (paint-scene scene))
    (sdl:set-point-* (sdl-surface scene) :x 0 :y 0)
    (if (rectangle-to-redraw scene)
        (let* ((scene-rect (mapcar (lambda (value)
                                     (the fixnum (round value)))
                                   (rectangle-to-redraw scene)))
               (left (first scene-rect))
               (top (second scene-rect))
               (width (1+ (- (third scene-rect) (first scene-rect))))
               (height (1+ (- (fourth scene-rect) (second scene-rect))))
               (sdl-rect (sdl:rectangle :x left :y top
                                        :w width :h height
                                        :fp nil)))
          (sdl:set-clip-rect sdl-rect :surface lispbuilder-sdl:*default-display*)
          (sdl:blit-surface (sdl-surface scene) )
          (sdl:free sdl-rect)
          (sdl:clear-clip-rect lispbuilder-sdl:*default-display*)
          (setf (rectangle-to-redraw scene) nil)
          (sdl-cffi::sdl-update-rect (sdl::fp lispbuilder-sdl:*default-display*)
                                     left top width height))
        ;; else
        (progn
          (sdl:blit-surface (sdl-surface scene) lispbuilder-sdl:*default-display*)
          (sdl:update-display)))))

(defun translated-mods ()
  (mapcar #'sdl-translate-key (sdl:mods-down-p)))

(defun run-scene (scene &key window-options)
  (sdl:with-init ()
    (apply #'sdl:window (width scene) (height scene) window-options)
    (sdl-cffi:sdl-enable-key-repeat 80 80)
    (setf (sdl:frame-rate) 100)
    (init-scene scene #'render-scene)
    (lispbuilder-sdl:enable-unicode)
    (let ((*scene* scene)
          event-queue)
      (sdl:with-events ()
        (:quit-event () t)
        (:idle ()
               (handle-events scene (nreverse event-queue) #'render-scene)
               (setf event-queue nil)
               (run-step-functions scene)
               (execute-ui-tasks scene)
               (render-scene scene))
        (:mouse-motion-event (:state state :x x :y y :x-rel x-rel :y-rel y-rel)
                             (declare (ignore state))
                             (push (make-instance 'mouse-move-event
                                                  :mouse-x x
                                                  :mouse-y y
                                                  :mouse-rel-x x-rel
                                                  :mouse-rel-y y-rel
                                                  :modifiers (translated-mods))
                                   event-queue))
        (:mouse-button-down-event (:button button :state state :x x :y y)
                                  (declare (ignore state))
                                  (push (make-instance
                                         'mouse-button-event
                                         :mouse-x x
                                         :mouse-y y
                                         :mouse-button button
                                         :button-state :down
                                         :modifiers (translated-mods))
                                        event-queue))
        (:mouse-button-up-event (:button button :state state :x x :y y)
                                (declare (ignore state))
                                (push (make-instance 'mouse-button-event
                                                     :mouse-x x
                                                     :mouse-y y
                                                     :button-state :up
                                                     :mouse-button button
                                                     :modifiers (translated-mods))
                                      event-queue))
        (:key-down-event (:state state :scancode scancode :key key
                                 :mod mod :mod-key mod-key :unicode unicode)
                         (declare (ignore state scancode mod))
                         (push (make-instance
                                'key-event
                                :key (sdl-translate-key key)
                                :modifiers (mapcar #'sdl-translate-key mod-key)
                                :key-state :down
                                :unicode (if (= 0 unicode)
                                             nil
                                             (code-char unicode)))
                               event-queue))
        (:key-up-event (:state state :scancode scancode :key key
                               :mod mod :mod-key mod-key :unicode unicode)
                       (declare (ignore state scancode mod))
                       (push (make-instance
                              'key-event
                              :key (sdl-translate-key key)
                              :modifiers (mapcar #'sdl-translate-key mod-key)
                              :key-state :up
                              :unicode (if (= 0 unicode)
                                           nil
                                           (code-char unicode)))
                             event-queue))
        (:video-expose-event () (sdl:update-display))
        (:video-resize-event (:w w :h h)
                             (setf (width scene) w)
                             (setf (height scene) h)
                             (setf (sdl-surface scene) nil)
                             (setf (layedout scene) nil)
                             (invalidate-scene scene)
                             (setf (rectangle-to-redraw scene) nil)
                             (sdl:resize-window w h)
                             (render-scene scene t))))))

(defun sdl-translate-key (key)
  (intern (subseq (symbol-name key) 8) "KEYWORD"))
