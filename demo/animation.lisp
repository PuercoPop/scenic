(in-package :scenic.demo)

(defun rectangular (width height &key fill-color)
  (placer 0 0 
          (sizer
           (background fill-color
                       (filler))
           :width width :height height)))

;; * Easing animation

;; ** Examples

(defun anim-ex-1 ()
  (with-bindings
    (let ((comp (stack
                 (background (scenic::color "white")
                             (placeholder 500 500))
                 (rectangular :bind rect 100 100
                              :fill-color (scenic.draw:parse-color-or-pattern
                                           '(:rgba 0 0 0 0.4))))))
      (let ((scene (scenic:scene 600 600
                                 comp))
            (dest-x 500)
            (dest-y 500)
            (current-time 0)
            (duration 1000)
            (speed 10)
            (easing
             ;;#'scenic.animation::ease-in-quad
             ;;'scenic.animation::ease-out-quad
             ;;'scenic.animation::ease-in-out-quad
             ;;#'scenic.animation::linear-tween
             ;;#'scenic.animation::ease-in-sin
             #'scenic.animation::ease-out-sin
              ;;'scenic.animation::ease-in-out-sin
              ))
        (flet ((animate (&key scene target &allow-other-keys)
                 (let ((x (funcall easing current-time 0 dest-x duration))
                       (y (funcall easing current-time 0 dest-y duration)))
                   (format t "x: ~A y: ~A~%" x y)
                   (setf (scenic::left rect)
                         x)
                   (setf (scenic::top rect)
                         y)
                   (incf current-time (/ duration (/ duration speed)))
                   (scenic::invalidate rect)
                   (scenic::mark-for-layout rect))))
          (scenic::add-step-function
           (scenic::step-function #'animate
                                  :target rect
                                  ;;:by 10
                                  :times nil
                                  :until (lambda ()
                                           (>= (scenic::left rect) 500)))
           scene)
          (scenic:run-scene scene :window-options '(:resizable t)))))))

;; (anim-ex-1)

;; Slinding drawer example
(defun anim-ex-sliding-drawer ()
  (with-bindings
    (let ((comp (stack
                 (background :bind bg cl-colors:+white+
                             (placeholder 500 500))
                 (translator :bind left-overlay-translator
                             (sizer :bind left-overlay
                                    (scenic.nui::rbox
                                        (:fill-color cl-colors:+darkblue+
                                                     :layout (scenic.nui::flow-layout
                                                              :orientation :vertical
                                                              :space-between-cells 5))
                                      (expander (button :bind hello
                                                        (uniform-padding
                                                         5
                                                         (aligner (label "Hello") :horizontal :right)))
                                                :expand-height nil)
                                      (expander (button (uniform-padding 5 (aligner (label "World"))))
                                                :expand-height nil))
                                    :width 100)
                             :x -90)
                 )))
      (let ((scene (scenic:scene 600 600
                                 comp))
            (dest-x 90)
            (dest-y 500)
            (current-time 0)
            (duration 1000)
            (left-overlay-visible nil)
            (left-overlay-opening nil)
            (easing
             ;;#'scenic.animation::ease-in-quad
             ;;'scenic.animation::ease-out-quad
             ;;'scenic.animation::ease-in-out-quad
             ;;#'scenic.animation::linear-tween
             ;;#'scenic.animation::ease-in-sin
             ;;#'scenic.animation::ease-out-sin
             'scenic.animation::ease-in-out-sin
              ))
        (flet ((show-left-sidebar (&key scene target &allow-other-keys)
                 (let ((w (funcall easing current-time 0 dest-x duration)))
                   (setf (horizontal-offset left-overlay-translator)
                         (- (- 90 w)))
                   (incf current-time (/ duration 10))
                   (scenic::invalidate (scenic::parent left-overlay-translator))

                   ))
               (hide-left-sidebar (&key scene target &allow-other-keys)
                 (let ((w (funcall easing current-time 0 dest-x duration)))
                   (setf (horizontal-offset left-overlay-translator)
                         (- (- 90 w) 90))
                   (incf current-time (/ duration 10))
                   (scenic::invalidate (scenic::parent left-overlay-translator))
                   ))
               )
          (scenic:add-event-handler left-overlay :mouse-move :bubble
                                    (lambda (target event)
                                      (when (and (not left-overlay-visible)
                                                 (not left-overlay-opening)
                                                 (< (mouse-x event) 1))
                                        (setf left-overlay-opening t)
                                        (scenic::add-step-function
                                         (scenic::step-function #'show-left-sidebar
                                                                :target left-overlay
                                                                :after 100
                                                                :by 10
                                                                :times nil
                                                                :until (lambda ()
                                                                         (zerop (horizontal-offset left-overlay-translator)))
                                                                :on-start
                                                                (lambda ()
                                                                  (setf left-overlay-opening t
                                                                        current-time 0))
                                                                :on-end
                                                                (lambda ()
                                                                  (setf left-overlay-visible t
                                                                        left-overlay-opening nil
                                                                        )))
                                         scene)
                                        )
                                      (when (and left-overlay-visible
                                                 (> (mouse-x event) 90)
                                                 (not left-overlay-opening))
                                        (setf left-overlay-opening t)
                                        (scenic::add-step-function
                                         (scenic::step-function #'hide-left-sidebar
                                                                :target left-overlay
                                                                :after 100
                                                                :by 10
                                                                :times nil
                                                                :until (lambda ()
                                                                         (<= (horizontal-offset left-overlay-translator) -90))
                                                                :on-start
                                                                (lambda ()
                                                                  (setf left-overlay-opening t
                                                                        current-time 0))
                                                                :on-end (lambda ()
                                                                          (setf left-overlay-visible nil
                                                                                left-overlay-opening nil)
                                                                          ))
                                         scene)



                                        )))
          (scenic:run-scene scene :window-options '(:resizable t  :title "Drawer example")))))))

;; (anim-ex-sliding-drawer)


(defun random-color ()
  (cl-colors:rgb (/ (random 255) 255)
                 (/ (random 255) 255)
                 (/ (random 255) 255)))

(defun anim-ex-bounce (&key (count 5) by)
  (let ((rects (loop
                  repeat count
                  for x := (random 500)
                  for y := (random 500)
                  collect
                    (let ((rect (rectangular 100 100
                                             :fill-color (random-color))))
                      (setf (scenic::left rect) x
                            (scenic::top rect) y)
                      rect)))
        (rects-updated 0))
    (with-bindings
      (let ((comp (make-instance 'stack
                                 :children
                                 (list*
                                  (make-instance 'clickable
                                                 :bind clickable
                                                 :child
                                                 (background cl-colors:+white+
                                                             (placeholder 500 500)))
                                  rects
                                  ))))
        (let ((scene (scenic:scene 600 600
                                   comp))
              (speed nil))

          (flet ((rect-animator (rect)
                   (let ((x (scenic::left rect))
                         (y (scenic::top rect))
                         (x-direction (nth (random 2) (list :left :right)))
                         (y-direction (nth (random 2) (list :down :up))))
                     (flet ((update-rect-pos ()
                              (when (<= x 0)
                                (setf x-direction :right))
                              (when (>= x 500)
                                (setf x-direction :left))
                              (ecase x-direction
                                (:right (incf x 1))
                                (:left (decf x 1)))
                              (setf (scenic::left rect) x)
                              (when (<= y 0)
                                (setf y-direction :down))
                              (when (>= y 500)
                                (setf y-direction :up))
                              (ecase y-direction
                                (:up (decf y 1))
                                (:down (incf y 1)))
                              (setf (scenic::top rect) y)))
                       (lambda (&key scene target &allow-other-keys)
                         (update-rect-pos)
                         (incf rects-updated)
                         (when (= rects-updated count)
                           (setf rects-updated 0)
                           (scenic::invalidate (scenic::parent rect)))
                         (scenic::mark-for-layout rect)
                         )))))
            (scenic::add-event-handler clickable :click nil
                                       (lambda (object event)
                                         (loop for rect in rects
                                            do
                                              (scenic::add-step-function
                                               (scenic::step-function (rect-animator rect)
                                                                      :target rect
                                                                      :by by
                                                                      :times nil)
                                               scene))))
            (scenic:run-scene scene :window-options '(:resizable t))))))))

;; (anim-ex-bounce)


(defun anim-ex-translate ()
  (with-bindings
    (let* ((comp (stack
                  (background (scenic::color "white")
                              (filler))
                  (placer 200 200
                          (adjuster (button :bind start-btn
                                            (uniform-padding 20 (label "Start")))))
                  (rectangular :bind rect 100 100
                               :fill-color (scenic.draw:parse-color-or-pattern
                                            '(:rgba 0 0 0 0.4)))))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler start-btn :click nil
                                (lambda (&rest args)
                                  (scenic.animation::translate
                                   rect
                                   :to-x 500
                                   :to-y 500
                                   :easing 'scenic.animation::ease-out-sin
                                   :duration 1000
                                   :speed 10
                                   ;; :on-change (lambda (&rest args)
                                   ;;              (format t "changed")
                                   ;;              (scenic::invalidate (scenic:parent rect))
                                   ;;              (scenic::mark-for-layout rect))
                                   :on-start (lambda (&rest args)
                                               (format t "Animation started~%"))
                                   :on-stop (lambda (&rest args)
                                              (format t "Animation stopped~%")))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-translate)

(defun anim-ex-sequence ()
  (with-bindings
    (let* ((comp (stack
                  (background (scenic::color "white")
                              (filler))
                  (placer 200 200
                          (adjuster (button :bind start-btn
                                            (uniform-padding 20 (label "Start")))))
                  (rectangular :bind rect 100 100
                               :fill-color (scenic.draw:parse-color-or-pattern
                                            '(:rgba 0 0 0 0.4)))))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler
       start-btn :click nil
       (lambda (&rest args)
         (scenic.animation::in-sequence
             (:on-start (lambda (&rest args)
                          (format t "Animation started~%"))
                        :on-stop (lambda (&rest args)
                                   (format t "Animation stopped~%")))
           (make-instance 'scenic.animation::translation-animation
                          :target rect
                          :to-x 100
                          :to-y 100
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10)
           (make-instance 'scenic.animation::translation-animation
                          :target rect
                          :to-x 100
                          :to-y 200
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-sequence)

(defun anim-ex-parallel ()
  (with-bindings
    (let* ((comp (stack
                  (background (scenic::color "white")
                              (filler))
                  (placer 200 200
                          (adjuster (button :bind start-btn
                                            (uniform-padding 20 (label "Start")))))
                  (rectangular :bind rect 100 100
                               :fill-color (scenic.draw:parse-color-or-pattern
                                            '(:rgba 0 0 0 0.4)))))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler
       start-btn :click nil
       (lambda (&rest args)
         (scenic.animation::in-parallel
             (:on-start (lambda (&rest args)
                          (format t "Animation started~%"))
                        :on-stop (lambda (&rest args)
                                   (format t "Animation stopped~%")))
           (make-instance 'scenic.animation::translation-animation
                          :target rect
                          :to-x 100
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10)
           (make-instance 'scenic.animation::translation-animation
                          :target rect
                          :to-y 200
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-parallel)

(defun anim-ex-fade ()
  (with-bindings
    (let* ((comp (stack
                  (background cl-colors:+black+
                              (filler))
                  (glass :bind image
                         1
                         (sizer
                          (image (asdf:system-relative-pathname
                                  :scenic
                                  "icons/lisplogo_warning_256.png"))
                          :width 165 :height 256))
                  (placer 300 300
                          (adjuster
                           (pack (:space-between-cells 2)
                                 (button :bind fadeout-btn
                                         (uniform-padding 20 (label "Fade out")))
                                 (button :bind fadein-btn
                                         (uniform-padding 20 (label "Fade in"))))))))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler fadeout-btn :click nil
                                (lambda (&rest args)
                                  (scenic.animation::fadeout
                                   image
                                   :easing 'scenic.animation::ease-out-sin
                                   :duration 1000
                                   :speed 10
                                   :on-start (lambda (&rest args)
                                               (format t "Fadeout started~%"))
                                   :on-stop (lambda (&rest args)
                                              (format t "Fadeout stopped~%")))))
      (scenic:add-event-handler fadein-btn :click nil
                                (lambda (&rest args)
                                  (scenic.animation::fadein
                                   image
                                   :easing 'scenic.animation::ease-out-sin
                                   :duration 1000
                                   :speed 10
                                   :on-start (lambda (&rest args)
                                               (format t "Fadein started~%"))
                                   :on-stop (lambda (&rest args)
                                              (format t "Fadein stopped~%")))))
      
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-fade)

(defun anim-ex-fade-transition ()
  (with-bindings
    (let* ((comp (stack
                  (background cl-colors:+white+
                              (filler))
                  (glass :bind img-1
                         1
                         (sizer
                          (image (asdf:system-relative-pathname
                                  :scenic
                                  "demo/photos/tiger.png"))
                          :width 600 :height 300))
                  (glass :bind img-2
                         0
                         (sizer
                          (image (asdf:system-relative-pathname
                                  :scenic
                                  "demo/photos/sea.png"))
                          :width 600 :height 300))
                  (placer 350 350
                          (adjuster
                           (button :bind transition-btn
                                   (uniform-padding 20 (label "Run")))
                                 ))))
           (scene (scenic:scene 700 600
                                comp)))
    (let ((ready t)
          (from-img img-1)
          (to-img img-2))
      (scenic:add-event-handler
       transition-btn
       :click nil
       (lambda (&rest args)
         (when ready
           (scenic.animation:in-parallel
               (:on-start (lambda (&rest args)
                            (setf ready nil))
                          :on-stop (lambda (&rest args)
                                     (let ((img from-img))
                                       (setf from-img to-img)
                                       (setf to-img img))
                                     (setf ready t)))
           (make-instance 'scenic.animation:fade-animation
                          :target from-img
                          :to-opacity 0
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10)
           (make-instance 'scenic.animation:fade-animation
                          :target to-img
                          :from-opacity 0
                          :to-opacity 1
                          :easing 'scenic.animation::ease-out-sin
                          :duration 1000
                          :speed 10)))))      
      (scenic:run-scene scene :window-options '(:resizable t))))))

;; (anim-ex-fade-transition)

(defun anim-ex-property ()
  (with-bindings
    (let* ((comp (stack
                  (background cl-colors:+white+
                              (filler))
                  (rectangular :bind rect
                               150 150
                               :fill-color cl-colors:+red+)
                  (placer 300 300
                          (adjuster
                           (button :bind start-btn
                                   (uniform-padding 20 (label "Start")))))))
           (scene (scenic:scene 600 600
                                comp)))
      (flet ((set-color (value obj)
               (setf (fill-color obj)
                     (cl-colors:rgb 
                      (cl-colors:rgb-red (fill-color obj))
                      value
                      (cl-colors:rgb-blue (fill-color obj))))
               (scenic:invalidate obj)))
        (scenic:add-event-handler start-btn :click nil
                                  (lambda (&rest args)
                                    (scenic.animation:start-animation 'scenic.animation:property-animation
                                                     :target (child (child rect))
                                                     :property-writer #'set-color
                                                     :start-value 0
                                                     :end-value 1
                                                     :duration 2000
                                                     :speed 10
                                                     :on-change (lambda (&rest args)
                                                                  (scenic:invalidate (child (child rect))))
                                                     :on-start (lambda (&rest args)
                                                                 (format t "Property animation started~%"))
                                                     :on-stop (lambda (&rest args)
                                                                (format t "Property animation stopped~%"))))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-property)

(defun anim-ex-resize ()
  (with-bindings
    (let* ((comp (stack
                  (background (scenic::color "white")
                              (filler))
                  (placer 200 200
                          (adjuster (button :bind start-btn
                                            (uniform-padding 20 (label "Start")))))
                  (sizer :bind rect
                   (background cl-colors:+blue+
                               (filler))
                   :width 100 :height 100)))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler start-btn :click nil
                                (lambda (&rest args)
                                  (scenic.animation:resize
                                   rect
                                   :to-width 200
                                   :to-height 350
                                   :easing 'scenic.animation::ease-out-sin
                                   :duration 1000
                                   :speed 20
                                   ;; :on-change (lambda (&rest args)
                                   ;;              (format t "changed")
                                   ;;              (scenic::invalidate (scenic:parent rect))
                                   ;;              (scenic::mark-for-layout rect))
                                   :on-start (lambda (&rest args)
                                               (format t "Animation started~%"))
                                   :on-stop (lambda (&rest args)
                                              (format t "Animation stopped~%")))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-resize)

(defun anim-ex-resize-2 ()
  (with-bindings
    (let* ((comp (stack
                  (background (scenic::color "white")
                              (filler))
                  (placer 200 200
                          (adjuster (button :bind start-btn
                                            (uniform-padding 20 (label "Start")))))
                  (sizer :bind sizer
                         (glass 1 :bind glass
                                (image (asdf:system-relative-pathname
                                        :scenic
                                        "icons/lisplogo_warning_256.png")))
                         :width 0 :height 0)))
           (scene (scenic:scene 600 600
                                comp)))
      (scenic:add-event-handler start-btn :click nil
                                (lambda (&rest args)
                                  (scenic.animation:in-parallel ()
                                    (make-instance 'scenic.animation:resize-animation
                                                   :target sizer
                                                   :to-width 200
                                                   :to-height 300
                                                   :easing 'scenic.animation::ease-out-sin
                                                   :duration 100
                                                   :speed 10)              
                                    (make-instance 'scenic.animation:fade-animation
                                                   :target glass
                                                   :from-opacity 0
                                                   :to-opacity 1
                                                   :easing 'scenic.animation::ease-out-sin
                                                   :duration 100
                                                   :speed 10))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (anim-ex-resize-2)
