(ql:quickload :cl-propagators)

(in-package :scenic.demo)

(defclass ui-cell (propagators:reactive-cell)
  ())

(defmethod scenic-utils::value ((cell propagators:reactive-cell))
  (propagators:content cell))

(defmethod (setf scenic-utils::value) (value (cell propagators:reactive-cell))
  (propagators:add-content cell value))

(defmethod scenic-utils::value-model-p ((cell propagators:reactive-cell))
  t)

(propagators:initialize-scheduler 'propagators:reactive-scheduler)

(defun propagators-ex-1 ()
  "(propagators-ex-1)"
  (propagators:let-cells
      ((slider-position 0 :type 'ui-cell)
       (label "" :type 'ui-cell :test 'equalp))
    (propagators:propagate (slider-position) label
      (princ-to-string slider-position))
    (propagators:propagate (label) slider-position
      (read-from-string label))
    (let ((comp (background cl-colors:+white+
                            (scenic.nui::flow-row (:spacing 5)
                              (sizer
                               (make-instance 'slider
                                              :orientation :horizontal
                                              :min-value 0
                                              :max-value 50
                                              :page-size 1
                                              :current-min-position slider-position)
                               :width 100 :height 20)
                              (label label)))))
      (run-scene (scene 400 400 comp)))))
