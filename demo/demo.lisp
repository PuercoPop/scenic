(in-package :scenic.demo)

(defun run-widget (widget &key (title "Scenic") (resizable nil))
  (let ((scene (scenic:scene 400 400 widget)))
    (scenic:run-scene scene :window-options (list :title title :resizable resizable))))

(defun buttons ()
  (let* (push-button
         toggle-button
         (buttons
          (stack
           (background (list 1.0 1.0 1.0)
                       (filler))
           (uniform-padding
            5
            (vertical-pack
             0
             '(:auto)
             (list
              (horizontal-pack 10
                               '(:auto :auto)
                               (list
                                (setf push-button
                                      (button-text "Push Button"))
                                (setf toggle-button
                                      (toggle "Toggle Button"))))))))))
    (scenic:add-event-handler push-button :mouse-move :cascade
                              (lambda (object event)
                                (scenic:test-channel-write
                                 (format nil "button mouse move: ~a ~a~%" object event))))
    (scenic:add-event-handler push-button :mouse-enter :cascade
                              (lambda (object event)
                                (scenic:test-channel-write
                                 (format nil "button enter: ~a ~a~%" object event))))
    (scenic:add-event-handler push-button :mouse-leave :cascade
                              (lambda (object event)
                                (scenic:test-channel-write
                                 (format nil "button mouse leave: ~a ~a~%" object event))))
    (scenic:add-event-handler push-button :mouse-button-down :cascade
                              (lambda (object event)
                                (scenic:test-channel-write
                                 (format nil "button mouse button-down: ~a ~a~%"
                                         object event))))
    (scenic:add-event-handler push-button :mouse-button-up :cascade
                              (lambda (object event)
                                (scenic:test-channel-write
                                 (format nil "button mouse button-up: ~a ~a~%"
                                         object event))))
    (scenic:add-event-handler push-button :click nil
                              (lambda (object event)
                                (declare (ignore object event))
                                (scenic:test-channel-write
                                 (format nil "push button clicked"))))
    (scenic:add-event-handler toggle-button :state-changed nil
                              (lambda (object event)
                                (declare (ignore object event))
                                (scenic:test-channel-write
                                 (format nil "toggle button ~a"
                                         (if (scenic:state toggle-button)
                                             "on"
                                             "off")))))
    buttons
    ))

(defun ext-layout ()
  (let ((content
         (horizontal-pack* 0 '((1 :ext) (3 :ext))
                           (vertical-pack* 300 nil
                                           (background (color cl-colors:+lightcyan+)
                                                       (uniform-padding 10
                                                                        (filler))))
                           (vertical-pack* 700 nil
                                           (uniform-padding 10
                                                            (buttons))))))

    (let ((scene (scenic:scene 1000 600
                               content
                               )))
      (scenic:run-scene scene))))

(defun abs-pos ()
  (let ((content (background '(1.0 0.2 0.2)
                             (uniform-padding 10
                                              (filler)))))
    (let ((scene (scenic:scene 400 400 content)))
      (scenic:run-scene scene))))

(defun frameless-window ()
  (let ((content (background '(1.0 0.2 0.2)
                             (uniform-padding 10
                                              (filler)))))
    (let ((scene (scenic:scene 400 400 content)))
      (scenic:run-scene scene :window-options '(:no-frame t)))))

(defun modal ()
  (let ((content (background '(1.0 0.2 0.2)
                             (uniform-padding 10
                                              (filler)))))
    (let ((scene (scenic:scene 400 400 content)))
      (scenic:run-scene scene :window-options '(:modal t)))))

(defun sidebar (&rest actions)
  (let ((buttons (mapcar (lambda (action)
                           (let ((button (button-text (first action))))
                             (scenic:add-event-handler button :click nil
                                                       (second action))
                             button))
                         actions)))
    ;(scenic-helpers:scroll-view-auto
     (stack
      (background (color cl-colors:+gray+)
                  (filler))
      (uniform-padding
       0
       (simple-vertical-pack
        0
        (mapcar (lambda (btn)
                  (expander btn :expand-width t :expand-height nil))
                buttons))))
     ;:always-vertical-scrollbar nil
     ;:always-horizontal-scrollbar nil)
  ))

(defun fonts-demo ()
  (flet ((render-font (font)
           (stack
            (background (color cl-colors:+white+)
                        (filler))
            (uniform-padding
             5
             (horizontal-pack
              0 '(:auto)
              (list
               (vertical-pack 0
                              '(:auto :auto :auto :auto :auto)
                              (list
                               (label font :size 10 :face font)
                               (label font :size 20 :face font)
                               (label font :size 30 :face font)
                               (label font :size 40 :face font)
                               (label font :size 50 :face font)))))))))
    (let ((fonts (list "Helvetica" "LM Roman 8" "Courier"
                       "Arial" "DejaVu Sans" "Comic Sans MS")))
      (let*
          ((container (make-instance 'scenic::container1))
           (content
            (horizontal-pack*
             0 '((1 :ext) (3 :ext))
             (apply #'sidebar
                    (mapcar (lambda (font)
                              (list font (lambda (button event)
                                           (setf (child container)
                                                 (render-font font)))))
                            fonts))
             container)))
        (setf (child container)
              (render-font "Helvetica"))
        (run-widget content :title "Fonts" :resizable t)))))

(scenic.style:defstylesheet styles-demo
  ("background"
   :background-color "blue")
  ("background#background-1"
   :background-color "blue"))

(import '(scenic-grid::grid scenic::button scenic::stack))

(defun styles-demo ()
  ;; We need to adjust the package because the CSS doesn't support
  ;; specifying the package for the type comparisons
  (let ((*package* (find-package :scenic.demo)))
    (let ((buttons (buttons)))
      (scenic.style:find-widgets "button" buttons)
      (scenic.style:find-widgets "stack button" buttons)
      (scenic.style:find-widgets "grid > button" buttons)
      (scenic.style:apply-stylesheet (scenic.style:find-stylesheet 'styles-demo) buttons)
      (run-widget buttons :title "Styles"))))

(defun fonts-demo-page ()
  (let ((btn
         (button (uniform-padding 5 (label "Fonts demo")))))
    (add-event-handler btn :click nil
                       (lambda (object event)
                         (fonts-demo)))
    (scenic::adjuster btn)))

(defun buttons-demo-page ()
  (let ((buttons (list (cons "Ex 1" 'scenic.nui::button-ex-1)
                       (cons "Ex 2" 'scenic.nui::button-ex-2)
                       (cons "Ex 3" 'scenic.nui::button-ex-3)
                       (cons "Ex 4" 'scenic.nui::button-ex-4)
                       (cons "SVG Toolbar" (lambda ()
                                             (run-widget (scenic.nui::svg-toolbar-ex))))
                       (cons "PNG Toolbar" (lambda ()
                                             (run-widget (scenic.nui::png-toolbar-ex)))))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun styles-demo-page ()
  (scenic.nui::flow-col (:spacing 5)
    (button (uniform-padding 5 (label "Styles demo"))
            :on-click (lambda (object event)
                        (styles-demo)))))

(defun checkbox-demo-page ()
  (let ((buttons (list (cons "Ex 1" 'scenic.nui::checkbox-ex-1)
                       (cons "Ex 2" 'scenic.nui::checkbox-ex-2)
                       (cons "Ex 3" 'scenic.nui::frame-ex-1)
                       (cons "Ex 4" 'scenic.nui::frame-ex-2)
                       (cons "Ex 5" 'scenic.nui::frame-ex-3)
                       )))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun tabview-demo-page ()
  (let ((buttons (list (cons "Example" 'scenic.nui::tabview-ex-1))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun rbox-demo-page ()
  (let ((buttons (list (cons "Ex1" 'scenic.nui::rbox-ex-grid)
                       (cons "Ex2" 'scenic.nui::rbox-ex-grid-1))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun dropdown-demo-page ()
  (let ((buttons (list (cons "Ex1" 'scenic.nui::dropdown-ex-1)
                       (cons "Ex2" 'scenic.nui::dropdown-ex-2)
                       (cons "Select Ex1" 'scenic.nui::select-ex-1))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun listview-demo-page ()
  (let ((buttons (list (cons "Ex1" 'scenic.nui::listview-ex-1)
                       (cons "Ex2" 'scenic.nui::listview-ex-2)
                       )))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun popup-demo-page ()
  (let ((buttons (list (cons "Ex1" 'scenic.nui::popup-ex-1)
                       (cons "Ex2" 'scenic.nui::popup-ex-2)
                       (cons "Ex3" 'scenic.nui::popup-ex-3)
                       (cons "Tooltip" 'scenic.nui::tooltip-ex-1)
                       (cons "Info" 'scenic.nui::info-dialog-test)
                       (cons "Warning" 'scenic.nui::warning-dialog-test)
                       (cons "Question" 'scenic.nui::question-dialog-test))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun textbox-demo-page ()
  (let ((buttons (list (cons "Ex1" 'scenic.nui::textbox-ex-1))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun animations-demo-page ()
  (let ((buttons (list (cons "Basic" 'anim-ex-1)
                       (cons "Sliding Drawer" 'anim-ex-sliding-drawer)
                       (cons "Image slider" 'slider-ex-1)
                       (cons "Sliding menu" 'scenic-test::sliding-menu-1)
                       (cons "Bounce" 'anim-ex-bounce)
                       (cons "Sprites" 'scenic::sprite-test)
                       (cons "In sequence" 'anim-ex-sequence)
                       (cons "In parallel" 'anim-ex-parallel)
                       (cons "Translate" 'anim-ex-translate)
                       (cons "Fade" 'anim-ex-fade)
                       (cons "Fade transition" 'anim-ex-fade-transition)
                       (cons "Property" 'anim-ex-property)
                       (cons "Resize" 'anim-ex-resize)
                       (cons "Resize and fade" 'anim-ex-resize-2))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun tasks-demo-page ()
  (let ((buttons (list (cons "Progress bar 1" 'progress-bar-demo-1)
                       (cons "Progress bar 2" 'progress-bar-demo-2)
                       (cons "Progress bar async" 'progress-bar-async-demo-1)
                       (cons "N progress bars async" (lambda ()
                                                       (dotimes (x 3)
                                                         (progress-bar-async-demo-1)))))))
    (scenic-helpers:simple-vertical-pack
     5
     (mapcar (lambda (x)
               (button (uniform-padding 5 (label (car x)))
                       :on-click (lambda (object event)
                                   (funcall (cdr x)))))
             buttons))))

(defun tests-demo-page ()
  (let ((buttons (list (cons "Background clear"
                             (scenic-test::background-clear))
                       (cons "Colored rectangles"
                             (scenic-test::colored-rectangles))
                       (cons "Hello world"
                             (scenic-test::hello-world))
                       (cons "Buttons"
                             (scenic-test::buttons))
                       (cons "Slider"
                             (scenic-test::slider))
                       (cons "Scrollbar"
                             (scenic-test::scrollbars))
                       (cons "Icon"
                             (scenic-test::icon))
                       (cons "Text Baseline Alignment"
                             (scenic-test::text-baseline-alignment))
                       (cons "Vertical pack"
                             (scenic-test::vertical-pack-layout-options))
                       (cons "Horizontal pack"
                             (scenic-test::horizontal-pack-layout-options))
                       (cons "Grid basic"
                             (scenic-test::grid-basic))
                       (cons "Grid offset"
                             (scenic-test::grid-offset))
                       (cons "Grid spans"
                             (scenic-test::grid-spans))
                       (cons "Grid layout"
                             (scenic-test::grid-layout-options))
                       ;;(test-scene (grid-layout-options-2))
                       ;;(test-scene (grid-layout-options-3))
                       (cons "Aligner"
                             (scenic-test::aligner-1))
                       (cons "Translator"
                             (scenic-test::translator-1))
                       (cons "Scaler"
                             (scenic-test::scaler-1))
                       (cons "Rotator"
                             (scenic-test::rotator-1))
                       (cons "Clipper"
                             (scenic-test::clipper-1))
                       (cons "Glass"
                             (scenic-test::glass-1))
                       ;;(test-scene (henchman-1))
                       ;;(test-scene (henchman-glass))
                       (cons "Scroll"
                             (scenic-test::scroll-view-1))
                       (cons "Scroll 2"
                             (scenic-test::scroll-view-2))
                       (cons "Scroll Hit Test"
                             (scenic-test::scroll-view-hittest))
                       (cons "Scroll Mouse Adjust"
                             (scenic-test::scroll-view-mouse-adjust))
                       (cons "TextBox 1"
                             (scenic-test::textbox-1))
                       (cons "TextBox 2"
                             (scenic-test::textbox-2))
                       (cons "Checkbox"
                             (scenic-test::checkbox-1))
                       (cons "Radio"
                             (scenic-test::radio-button-1))
                       (cons "Simple Box"
                             (scenic-test::simple-boxes))
                       )))
    (scenic-helpers:scroll-view-auto 
     (scenic-helpers:simple-vertical-pack
      5
      (mapcar (lambda (x)
                (button (uniform-padding 5 (label (car x)))
                        :on-click (lambda (object event)
                                    (scenic-test::test-scene (cdr x)))))
              buttons))
     :always-horizontal-scrollbar nil)))

(defun demo ()
  "(demo)"
  (let ((demos (list
                (list :title "Fonts" :component (fonts-demo-page))
                (list :title "Styles" :component (styles-demo-page))
                (list :title "Buttons" :component (buttons-demo-page))
                (list :title "Checkboxes" :component (checkbox-demo-page))
                (list :title "Dropdown" :component (dropdown-demo-page))
                (list :title "Listview" :component (listview-demo-page))
                (list :title "Tabview" :component (tabview-demo-page))
                (list :title "RBox" :component (rbox-demo-page))
                (list :title "Textbox" :component (textbox-demo-page))
                (list :title "Popup" :component (popup-demo-page))
                (list :title "Animations" :component (animations-demo-page))
                (list :title "Tasks" :component (tasks-demo-page))
                (list :title "Tests" :component (tests-demo-page))
                )))
    (let*
        ((container (make-instance 'scenic::container1))
         (content
          (horizontal-pack*
           0 '((1 :ext) (3 :ext))
           (apply #'sidebar
                  (mapcar (lambda (demo)
                            (list (getf demo :title)
                                  (lambda (button event)
                                    (setf (child container)
                                          (stack
                                           (background cl-colors:+white+ (filler))
                                           (uniform-padding 5
                                                            (getf demo :component)))))))
                          demos))
           container)))
      (setf (child container)
            (stack
             (background cl-colors:+white+ (filler))
             (uniform-padding 5
                              (getf (first demos) :component))))
      
      (scenic::reset-threads)
      (scenic::reset-ui-tasks)
      (scenic:start-async-task-processors)
      (scenic.nui::run-component
       content
       :title "Scenic Demo"
       :resizable t
       :width 700
       :height 500))))

(format t "~%~%==================~%")
(format t "SCENIC DEMO LOADED~%")
(format t "Run with (scenic.demo:demo)~%")
(format t "==================~%~%~%")

