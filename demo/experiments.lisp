(in-package :scenic.demo)

(defun view-sdl-image (filename)
  (let ((c (stack (background cl-colors:+white+
                              (filler))
                  (sizer (scenic::sdl-image (asdf:system-relative-pathname
                                             :scenic (format nil "demo/photos/~A" filename)))
                         :width 600 :height 350))))
    (run-scene (scene 600 350 c))))

;;(view-sdl-image "woman.jpg")
;; (view-sdl-image "tiger.jpg")
;; (view-sdl-image "aircraft.jpg")
;; (view-sdl-image "sea.jpg")

(defun view-image (filename)
  (let ((c (stack (background cl-colors:+white+
                              (filler))
                  (sizer (image (asdf:system-relative-pathname
                                 :scenic (format nil "demo/photos/~A" filename)))
                         :width 600 :height 350))))
    (run-scene (scene 600 350 c))))

;; (view-image "woman.png")
;; (view-image "tiger.png")
;; (view-image "aircraft.png")
;; (view-image "sea.png")

;; (let ((c (stack (background cl-colors:+white+
;;                             (filler))
;;                 (sizer
;;                  (O-clipper
;;                   (image (asdf:system-relative-pathname :scenic "icons/lisplogo_warning2_256.png")))
;;                   :width 150 :height 150))))
;;   (run-scene (scene 200 200 c)))

(defun round-button (icon click-handler)
  (with-bindings
    (sizer :bind btn
           (scenic.nui:rbox :bind rbtn
                            (:corner-radius 15
                                            :fill-color cl-colors:+white+
                                            :border (scenic.style:border 2 :solid cl-colors:+darkgray+))
                            (expander
                             (aligner
                              (sizer (fa-icon icon)
                                     :width 15 :height 15))))
           :width 30 :height 30)
    (add-event-handler rbtn :mouse-enter :bubble
                       (lambda (&rest args)
                         (setf (fill-color rbtn) cl-colors:+lightgray+)
                         (invalidate rbtn)))
    (add-event-handler rbtn :mouse-leave :bubble
                       (lambda (&rest args)
                         (setf (fill-color rbtn) cl-colors:+white+)
                         (invalidate rbtn)))
    (add-event-handler rbtn :mouse-button-down :bubble
                       (lambda (&rest args)
                         (setf (fill-color rbtn) cl-colors:+darkgray+)
                         (invalidate rbtn)
                         (funcall click-handler)))
    (add-event-handler rbtn :mouse-button-up :bubble
                       (lambda (&rest args)
                         (setf (fill-color rbtn) cl-colors:+lightgray+)
                         (invalidate rbtn)))
    btn))

(defun slider-ex-1 (&key (easing 'scenic.animation:linear-tween))
  (let ((images (list "aircraft.png" "sea.png" "tiger.png" "woman.png"))
        (image-index 0)
        (sliding nil))
    (with-bindings
      (labels ((set-slide-right (val translator)
                 ;;(format t "Set slide: ~A~%" (round val))
                 (setf (horizontal-offset translator) (round val))
                 (invalidate translator))
               (set-slide-left (val translator)
                 ;;(format t "Set slide: ~A~%" (round val))
                 (setf (horizontal-offset translator) (round val))
                 (invalidate translator))
               (slide-right (&rest args)
                 (when (and (< image-index (1- (length images)))
                            (not sliding))
                   (setf sliding t)
                   ;;(decf (horizontal-offset translator) 600)
                   (scenic.animation:start-animation
                    'scenic.animation:property-animation
                    :target translator
                    :property-writer #'set-slide-right
                    :easing easing
                    :start-value (horizontal-offset translator)
                    :end-value (+ (horizontal-offset translator) 600)
                    :duration 1000
                    :speed 20
                    :on-start (lambda (&rest args)
                                (format t "Sliding right~%"))
                    :on-stop (lambda (&rest args)
                               (format t "Slide end~%")
                               (setf sliding nil)))
                   (incf image-index)))
               (slide-left (&rest args)
                 (when (and (> image-index 0)
                            (not sliding))
                   (setf sliding t)
                   ;;(incf (scenic::horizontal-offset translator) 600)
                   (scenic.animation:start-animation
                    'scenic.animation:property-animation
                    :target translator
                    :property-writer #'set-slide-left
                    :easing easing
                    :start-value (horizontal-offset translator)
                    :end-value (- (horizontal-offset translator) 600)
                    :duration 1000
                    :speed 20
                    :on-start (lambda (&rest args)
                                (format t "Sliding left~%"))
                    :on-stop (lambda (&rest args)
                               (format t "Slide end~%")
                               (setf sliding nil))
                    )
                   (decf image-index))))
        (let ((c (stack (clipper
                         (translator :bind translator
                                     (make-instance 'pack
                                                    :orientation :horizontal
                                                    :children
                                                    (mapcar (lambda (filename)
                                                              (sizer
                                                               (image (asdf:system-relative-pathname
                                                                       :scenic (format nil "demo/photos/~A" filename)))
                                                               :width 600 :height 350))
                                                            images))))
                        (make-instance 'proportional-container
                                       :orientation :horizontal
                                       :children
                                       (list
                                        (expander
                                         (aligner (uniform-padding
                                                   5
                                                   (glass 0.5 (round-button "arrow_left" #'slide-left)))
                                                  :horizontal :left :vertical :center))
                                        (expander
                                         (aligner (uniform-padding
                                                   5
                                                   (glass 0.5 (round-button "arrow_right"
                                                                            #'slide-right)))
                                                  :horizontal :right :vertical :center))
                                        ))
                        )))

          (run-scene (scene 600 350 c)))))))

;; (slider-ex-1)
;; (slider-ex-1 :easing 'scenic.animation:ease-in-out-sin)
;; (slider-ex-1 :easing 'scenic.animation:ease-out-sin)
;; (slider-ex-1 :easing 'scenic.animation:ease-in-sin)
;; (slider-ex-1 :easing 'scenic.animation:ease-out-quad)
