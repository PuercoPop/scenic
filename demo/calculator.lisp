(in-package :scenic.demo)

(defun calculator ()
  (flet ((calc-button (label action)
           (let ((button (scenic.nui::nui-button-labelled label)))
             (scenic.nui::on button :click action)
             button)))
                             
    (scenic.nui::with-bindings
      (grid :bind numbers
            nil nil
            (loop for ns in '((7 8 9) (4 5 6) (1 2 3))
               collect
                 (cons :row
                       (loop for n in ns
                          collect
                            (list :cell (uniform-padding
                                         2
                                         (calc-button (princ-to-string n)
                                                      (let ((n0 n))
                                                        (lambda (btn ev)
                                                          (format t "~A" n0))))))))))
      (let ((scene (scenic:scene 200 200 numbers)))
        (scenic:run-scene scene)))))
